﻿using KirasaModelsDBLib;
using System;
using System.Globalization;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(Coord), targetType: typeof(string))]
    public class CoordsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
                if ((System.Convert.ToDouble(((Coord)value).Latitude) == -1) ||
                    (System.Convert.ToDouble(((Coord)value).Longitude) == -1)) 
                {
                    return "-";
                }
                if ((System.Convert.ToDouble(((Coord)value).Latitude) == -2) &&
                   (System.Convert.ToDouble(((Coord)value).Longitude) == -2)) 
                {
                    return string.Empty;
                }

            double dLat = ((Coord)value).Latitude < 0 ? ((Coord)value).Latitude * -1 : ((Coord)value).Latitude;
            double dLon = ((Coord)value).Longitude < 0 ? ((Coord)value).Longitude * -1 : ((Coord)value).Longitude;
            //}
            //catch { }

            return (System.Convert.ToString(dLat.ToString("0.000000")) + "   " +
                    System.Convert.ToString(dLon.ToString("0.000000")));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(sourceType: typeof(Coord), targetType: typeof(string))]
    public class LatitudeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            if ((System.Convert.ToDouble(((Coord)value).Latitude) == -1) ||
                (System.Convert.ToDouble(((Coord)value).Longitude) == -1))
            {
                return "-";
            }
            if ((System.Convert.ToDouble(((Coord)value).Latitude) == -2) &&
               (System.Convert.ToDouble(((Coord)value).Longitude) == -2))
            {
                return string.Empty;
            }

            double dLat = ((Coord)value).Latitude < 0 ? ((Coord)value).Latitude * -1 : ((Coord)value).Latitude;
            //double dLon = ((Coord)value).Longitude < 0 ? ((Coord)value).Longitude * -1 : ((Coord)value).Longitude;
            //}
            //catch { }

            return /*System.Convert.ToString*/(dLat.ToString("0.000000"));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(sourceType: typeof(Coord), targetType: typeof(string))]
    public class LongitudeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            if ((System.Convert.ToDouble(((Coord)value).Latitude) == -1) ||
                (System.Convert.ToDouble(((Coord)value).Longitude) == -1))
            {
                return "-";
            }
            if ((System.Convert.ToDouble(((Coord)value).Latitude) == -2) &&
               (System.Convert.ToDouble(((Coord)value).Longitude) == -2))
            {
                return string.Empty;
            }

            //double dLat = ((Coord)value).Latitude < 0 ? ((Coord)value).Latitude * -1 : ((Coord)value).Latitude;
            double dLon = ((Coord)value).Longitude < 0 ? ((Coord)value).Longitude * -1 : ((Coord)value).Longitude;
            //}
            //catch { }

            return System.Convert.ToString(dLon.ToString("0.000000"));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(sourceType: typeof(Coord), targetType: typeof(string))]
    public class E_WConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            if ((System.Convert.ToDouble(((Coord)value).Latitude) == -1) ||
                (System.Convert.ToDouble(((Coord)value).Longitude) == -1))
            {
                return "-";
            }
            if ((System.Convert.ToDouble(((Coord)value).Latitude) == -2) &&
               (System.Convert.ToDouble(((Coord)value).Longitude) == -2))
            {
                return string.Empty;
            }

            //double dLat = ((Coord)value).Latitude < 0 ? ((Coord)value).Latitude * -1 : ((Coord)value).Latitude;
            //double dLon = ((Coord)value).Longitude < 0 ? ((Coord)value).Longitude * -1 : ((Coord)value).Longitude;
            //}
            //catch { }

            if (((Coord)value).Longitude < 0)
                return "W ";
            else return "E ";

            //return System.Convert.ToString(dLon.ToString("0.000000"));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(sourceType: typeof(Coord), targetType: typeof(string))]
    public class N_SConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            if ((System.Convert.ToDouble(((Coord)value).Latitude) == -1) ||
                (System.Convert.ToDouble(((Coord)value).Longitude) == -1))
            {
                return "-";
            }
            if ((System.Convert.ToDouble(((Coord)value).Latitude) == -2) &&
               (System.Convert.ToDouble(((Coord)value).Longitude) == -2))
            {
                return string.Empty;
            }

            //double dLat = ((Coord)value).Latitude < 0 ? ((Coord)value).Latitude * -1 : ((Coord)value).Latitude;
            //double dLon = ((Coord)value).Longitude < 0 ? ((Coord)value).Longitude * -1 : ((Coord)value).Longitude;
            //}
            //catch { }

            if (((Coord)value).Latitude < 0)
                return "S ";
            else return "N ";

            //return System.Convert.ToString(dLon.ToString("0.000000"));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
