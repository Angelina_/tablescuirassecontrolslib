﻿using System;
using System.Globalization;
using System.Windows.Data;
using KirasaModelsDBLib;
using ValuesCorrectLib;

namespace ValuesConverter
{
    public class TypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string sType = string.Empty;

            //try
            //{

            if ((byte)value == 255)
                return sType;

            switch (value)
            {
                case TypeUAVRes.Ocusinc:
                    sType = "Mavic 2";
                    break;

                case TypeUAVRes.G3:
                    sType = "3G";
                    break;

                case TypeUAVRes.Lightbridge:
                    sType = "Phantom 3";
                    break;

                case TypeUAVRes.WiFi:
                    sType = "Mavic Air";
                    break;

                case TypeUAVRes.Unknown:
                    sType = "Unknown";
                    break;

                default:
                    sType = string.Empty;
                    break;
            }

            //}
            //catch { }

            return sType;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TypeMConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string sTypeM = string.Empty;

            //try
            //{

            if ((byte)value == 255)
                return sTypeM;

            switch (value)
            {
                case TypeM.Drone:
                    sTypeM = "Drone";
                    break;

                case TypeM.Unknown:
                    sTypeM = "Unknown";
                    break;

                default:
                    sTypeM = string.Empty;
                    break;
            }

            //}
            //catch { }

            return sTypeM;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TypeLConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string sTypeL = string.Empty;

            //try
            //{

            if ((byte)value == 255)
                return sTypeL;

            switch (value)
            {
                case TypeL.Friend:
                    sTypeL = "Friend";
                    break;

                case TypeL.Enemy:
                    sTypeL = "Enemy";
                    break;

                default:
                    sTypeL = string.Empty;
                    break;
            }

            //}
            //catch { }

            return sTypeL;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TypeSystemConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string sTypeSystem = string.Empty;

            //try
            //{

            if ((byte)value == 255)
                return sTypeSystem;

            switch (value)
            {
                case TypeSystem.CP_CuirasseM:
                    sTypeSystem = "CP_CuirasseM";
                    break;

                case TypeSystem.Martsinovich:
                    sTypeSystem = "Martsinovich";
                    break;

                case TypeSystem.Lyakh:
                    sTypeSystem = "Lyakh";
                    break;

                case TypeSystem.Manual:
                    sTypeSystem = "Manual";
                    break;

                case TypeSystem.Spectrum:
                    sTypeSystem = "Spectrum";
                    break;

                default:
                    sTypeSystem = string.Empty;
                    break;
            }

            //}
            //catch { }

            return sTypeSystem;

        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TypeOtherPointsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string sTypeSystem = string.Empty;

            //try
            //{

            if ((byte)value == 255)
                return sTypeSystem;

            switch (value)
            {
                case TypeOtherPoints.GrozaS:
                    sTypeSystem = SMeaning.meaningTypeOtherPoints;
                    break;

                default:
                    sTypeSystem = string.Empty;
                    break;
            }

            //}
            //catch { }

            return sTypeSystem;

        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TypePropGridConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            byte bRole = (byte)value;

            return (byte)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (TypeOtherPoints)System.Convert.ToByte(value);
        }
    }
}
