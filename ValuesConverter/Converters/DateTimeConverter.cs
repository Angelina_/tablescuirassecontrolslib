﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(DateTime), targetType: typeof(string))]
    public class DateTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //DateTime dt = ((DateTime)value);
            //string[] strDate = System.Convert.ToString(dt).Split(' ');

            string[] strDate = System.Convert.ToString(value).Split(' ');

            //try
            //{
            if ((strDate[0] == "01.01.0001" || strDate[0] == "01/01/0001" || strDate[0] == "01-01-0001" ||
                strDate[0] == "01.01.01" || strDate[0] == "01/01/01" || strDate[0] == "01-01-01") ||
                (strDate[0] == "00.00.0000" || strDate[0] == "00/00/0000" || strDate[0] == "00-00-0000" ||
                strDate[0] == "00.00.00" || strDate[0] == "00/00/00" || strDate[0] == "00-00-00"))
            {
                    return string.Empty;
            }
            //}
            //catch { }

            string strTime = ((DateTime)value).Hour.ToString("00") + ":" + ((DateTime)value).Minute.ToString("00") + ":" + ((DateTime)value).Second.ToString("00");// + "." + ((DateTime)value).Millisecond.ToString("000");

            return strTime;
            //return strDate[1];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
