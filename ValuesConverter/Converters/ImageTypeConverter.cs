﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(TypeUAVRes), targetType: typeof(Uri))]
    public class ImageTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            Uri uri = new Uri(@"pack://application:,,,/"
                              + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                              + ";component/"
                              + "Resources/NoImage.png", UriKind.Absolute);
            
            switch (value)
            {
                case TypeUAVRes.Ocusinc: // "Mavic 2"
                    uri = new Uri(@"pack://application:,,,/"
                              + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                              + ";component/"
                              + "Resources/Mavic 2.png", UriKind.Absolute);
                    break;

                case TypeUAVRes.G3: // "3G"
                    uri = new Uri(@"pack://application:,,,/"
                              + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                              + ";component/"
                              + "Resources/3G.png", UriKind.Absolute);
                    break;

                case TypeUAVRes.Lightbridge: // "Phantom 4"
                    uri = new Uri(@"pack://application:,,,/"
                              + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                              + ";component/"
                              + "Resources/Phantom 4.png", UriKind.Absolute);
                    break;

                case TypeUAVRes.WiFi: // "Mavic Air"
                    uri = new Uri(@"pack://application:,,,/"
                              + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                              + ";component/"
                              + "Resources/Mavic Air.png", UriKind.Absolute);
                    break;

                case TypeUAVRes.Unknown: // "DJI Phantom 3"
                    uri = new Uri(@"pack://application:,,,/"
                             + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                             + ";component/"
                             + "Resources/NoImage2.png", UriKind.Absolute);
                    break;

                default: // No image
                    uri = new Uri(@"pack://application:,,,/"
                              + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                              + ";component/"
                              + "Resources/NoImage.png", UriKind.Absolute);
                    break;
            }

            return uri;
            //}

            //catch { return null; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
