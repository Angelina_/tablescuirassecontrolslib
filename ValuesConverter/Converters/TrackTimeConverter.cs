﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ValuesConverter
{
  
    public class TrackTimeConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (System.Convert.ToInt32(values[2]) == -2) return string.Empty;

                TimeSpan timeSpan = System.Convert.ToDateTime(values[1]) - System.Convert.ToDateTime(values[0]);
                DateTime TrackTime = System.Convert.ToDateTime(timeSpan.ToString());

                string strTrackTime = TrackTime.Hour.ToString("00") + ":" + TrackTime.Minute.ToString("00") + ":"
                                      + TrackTime.Second.ToString(
                                          "00"); // + "." + TrackTime.Millisecond.ToString("000");

                return strTrackTime;

            }
            catch
            {
                return Binding.DoNothing;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
