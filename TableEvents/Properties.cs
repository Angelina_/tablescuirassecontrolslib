﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableEvents
{
    public class PropShowDialog
    {
        private static bool isShowDialogOpen = false;
        public static bool IsShowDialogOpen
        {
            get { return isShowDialogOpen; }
            set
            {
                if (isShowDialogOpen == value)
                    return;

                isShowDialogOpen = value;
            }
        }
    }

    public class PropNumUAV
    {
        //private static int selectedNumUAVRes = 2;// для отладки
        private static int selectedNumUAVRes = 0;
        public static int SelectedNumUAVRes
        {
            get { return selectedNumUAVRes; }
            set
            {
                if (selectedNumUAVRes == value)
                    return;

                selectedNumUAVRes = value;
            }
        }

        private static int selectedIdUAVRes = 0;
        public static int SelectedIdUAVRes
        {
            get { return selectedIdUAVRes; }
            set
            {
                if (selectedIdUAVRes == value)
                    return;

                selectedIdUAVRes = value;
            }
        }

        //private static bool isSelectedNumUAVRes = true; // для отладки
        private static bool isSelectedNumUAVRes = false;
        public static bool IsSelectedNumUAVRes
        {
            get { return isSelectedNumUAVRes; }
            set
            {
                if (isSelectedNumUAVRes == value)
                    return;

                isSelectedNumUAVRes = value;
            }
        }

        //private static int selectedNumUAVResArchive = 2;// для отладки
        private static int selectedNumUAVResArchive = 0;
        public static int SelectedNumUAVResArchive
        {
            get { return selectedNumUAVResArchive; }
            set
            {
                if (selectedNumUAVResArchive == value)
                    return;

                selectedNumUAVResArchive = value;
            }
        }

        private static int selectedIdUAVResArchive = 0;
        public static int SelectedIdUAVResArchive
        {
            get { return selectedIdUAVResArchive; }
            set
            {
                if (selectedIdUAVResArchive == value)
                    return;

                selectedIdUAVResArchive = value;
            }
        }

        //private static bool isSelectedNumUAVRes = true; // для отладки
        private static bool isSelectedNumUAVResArchive = false;
        public static bool IsSelectedNumUAVResArchive
        {
            get { return isSelectedNumUAVResArchive; }
            set
            {
                if (isSelectedNumUAVResArchive == value)
                    return;

                isSelectedNumUAVResArchive = value;
            }
        }

        //private static int selectedNumAeroscope = 2;// для отладки
        private static int selectedNumAeroscope = 0;
        public static int SelectedNumAeroscope
        {
            get { return selectedNumAeroscope; }
            set
            {
                if (selectedNumAeroscope == value)
                    return;

                selectedNumAeroscope = value;
            }
        }

        private static string selectedSerialNumAeroscope = string.Empty;
        public static string SelectedSerialNumAeroscope
        {
            get { return selectedSerialNumAeroscope; }
            set
            {
                if (selectedSerialNumAeroscope == value)
                    return;

                selectedSerialNumAeroscope = value;
            }
        }

        //private static bool isSelectedNumAeroscope = true; // для отладки
        private static bool isSelectedNumAeroscope = false;
        public static bool IsSelectedNumAeroscope
        {
            get { return isSelectedNumAeroscope; }
            set
            {
                if (isSelectedNumAeroscope == value)
                    return;

                isSelectedNumAeroscope = value;
            }
        }

        //private static int selectedNumSignalsUAV = 2;// для отладки
        private static int selectedNumSignalsUAV = 0;
        public static int SelectedNumSignalsUAV
        {
            get { return selectedNumSignalsUAV; }
            set
            {
                if (selectedNumSignalsUAV == value)
                    return;

                selectedNumSignalsUAV = value;
            }
        }

        private static int selectedIdSignalsUAV = 0;
        public static int SelectedIdSignalsUAV
        {
            get { return selectedIdSignalsUAV; }
            set
            {
                if (selectedIdSignalsUAV == value)
                    return;

                selectedIdSignalsUAV = value;
            }
        }
    }

    public class PropIsRecReceivingPoint
    {
        private static bool isRecAdd = false;
        public static bool IsRecAdd
        {
            get { return isRecAdd; }
            set
            {
                if (isRecAdd == value)
                    return;

                isRecAdd = value;
            }
        }

        private static bool isRecChange = false;
        public static bool IsRecChange
        {
            get { return isRecChange; }
            set
            {
                if (isRecChange == value)
                    return;

                isRecChange = value;
            }
        }
    }

    public class PropIsRecOtherPoints
    {
        private static bool isRecAdd = false;
        public static bool IsRecAdd
        {
            get { return isRecAdd; }
            set
            {
                if (isRecAdd == value)
                    return;

                isRecAdd = value;
            }
        }

        private static bool isRecChange = false;
        public static bool IsRecChange
        {
            get { return isRecChange; }
            set
            {
                if (isRecChange == value)
                    return;

                isRecChange = value;
            }
        }
    }
}
