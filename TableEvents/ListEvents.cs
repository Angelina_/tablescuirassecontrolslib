﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableEvents
{
    public class ListEvents : EventArgs
    {
        public List<TableUAVBase> Records { get; }

        public NameTable NameTable { get; private set; }

        public TypeUAVRes TypeUAVRes { get; private set; }

        public ListEvents(List<TableUAVBase> recs, TypeUAVRes type, NameTable name)
        {
            Records = recs;
            NameTable = name;
            TypeUAVRes = type;
            //object[] attrs = recs.GetType().GetCustomAttributes(typeof(InfoTableAttribute), false);
            //foreach (InfoTableAttribute info in attrs)
            //{
            //    NameTable = info.Name;
                
            //}
        }
    }
}
