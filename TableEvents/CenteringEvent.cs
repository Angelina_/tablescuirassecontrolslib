﻿
using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableEvents
{
    public class CenteringEvent : EventArgs
    {
        public Coord Coord { get; }

        public CenteringEvent(Coord coord)
        {
            Coord = coord;
        }
    }

}
