﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace ReceivingPointControl
{
    public partial class UserControlReceivingPoint : UserControl
    {
        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="listReceivingPoint"></param>
        public void UpdateReceivingPoint(List<TableReceivingPoint> listReceivingPoint)
        {
            try
            {
                if (listReceivingPoint == null)
                    return;

                ((GlobalReceivingPoint)DgvReceivingPoint.DataContext).CollectionReceivingPoint.Clear();

                CalculateDistance(ref listReceivingPoint);

                for (int i = 0; i < listReceivingPoint.Count; i++)
                {

                    ((GlobalReceivingPoint)DgvReceivingPoint.DataContext).CollectionReceivingPoint.Add(listReceivingPoint[i]);
                }

                AddEmptyRows();

            }
            catch { }
        }

        /// <summary>
        /// Расстояние между пунктами (1-2, 1-3, 1-4)
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private void CalculateDistance(ref List<TableReceivingPoint> list)
        {
            int ind1 = list.FindIndex(x => x.Id == 1);
            int ind2 = list.FindIndex(x => x.Id == 2);
            int ind3 = list.FindIndex(x => x.Id == 3);
            int ind4 = list.FindIndex(x => x.Id == 4);
           
            if(ind1 != -1) list[ind1].Antenna = -1.0F;

            if (ind1 != -1 && ind2 != -1)
            {
                double dist2Points1_2 = Bearing.ClassBearing.f_D_2Points(
                    list[ind1].Coordinates.Latitude,
                    list[ind1].Coordinates.Longitude,
                    list[ind2].Coordinates.Latitude,
                    list[ind2].Coordinates.Longitude, 1);

                list[ind2].Antenna = Convert.ToSingle(dist2Points1_2);
            }
            if (ind1 != -1 && ind3 != -1)
            {
                double dist2Points1_3 = Bearing.ClassBearing.f_D_2Points(
                    list[ind1].Coordinates.Latitude,
                    list[ind1].Coordinates.Longitude,
                    list[ind3].Coordinates.Latitude,
                    list[ind3].Coordinates.Longitude, 1);

                list[ind3].Antenna = Convert.ToSingle(dist2Points1_3);
            }
            if (ind1 != -1 && ind4 != -1)
            {
                double dist2Points1_4 = Bearing.ClassBearing.f_D_2Points(
                    list[ind1].Coordinates.Latitude,
                    list[ind1].Coordinates.Longitude,
                    list[ind4].Coordinates.Latitude,
                    list[ind4].Coordinates.Longitude, 1);

                list[ind4].Antenna = Convert.ToSingle(dist2Points1_4);
            }
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listReceivingPoint"></param>
        public void AddReceivingPoint(List<TableReceivingPoint> listReceivingPoint)
        {
            try
            {
                DeleteEmptyRows();

                CalculateDistance(ref listReceivingPoint);

                for (int i = 0; i < listReceivingPoint.Count; i++)
                {
                    int ind = ((GlobalReceivingPoint)DgvReceivingPoint.DataContext).CollectionReceivingPoint.ToList().FindIndex(x => x.Id == listReceivingPoint[i].Id);
                    if (ind != -1)
                    {
                        ((GlobalReceivingPoint)DgvReceivingPoint.DataContext).CollectionReceivingPoint[ind] = listReceivingPoint[i];
                    }
                    else
                    {
                        ((GlobalReceivingPoint)DgvReceivingPoint.DataContext).CollectionReceivingPoint.Add(listReceivingPoint[i]);
                    }
                }

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteReceivingPoint(TableReceivingPoint tableReceivingPoint)
        {
            try
            {
                int index = ((GlobalReceivingPoint)DgvReceivingPoint.DataContext).CollectionReceivingPoint.ToList().FindIndex(x => x.Id == tableReceivingPoint.Id);
                ((GlobalReceivingPoint)DgvReceivingPoint.DataContext).CollectionReceivingPoint.RemoveAt(index);

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearReceivingPoint()
        {
            try
            {
                ((GlobalReceivingPoint)DgvReceivingPoint.DataContext).CollectionReceivingPoint.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvReceivingPoint.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvReceivingPoint.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvReceivingPoint.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalReceivingPoint)DgvReceivingPoint.DataContext).CollectionReceivingPoint.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalReceivingPoint)DgvReceivingPoint.DataContext).CollectionReceivingPoint.RemoveAt(index);
                    }
                }

                List<TableReceivingPoint> list = new List<TableReceivingPoint>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    TableReceivingPoint strRP = new TableReceivingPoint
                    {
                        Id = -2,
                        Coordinates = new Coord() { Altitude = -2, Latitude = -2, Longitude = -2 },
                        Antenna = -2,
                        IsCetral = false,
                        TimeError = -2,
                        Note = string.Empty
                    };
                    list.Add(strRP);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalReceivingPoint)DgvReceivingPoint.DataContext).CollectionReceivingPoint.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalReceivingPoint)DgvReceivingPoint.DataContext).CollectionReceivingPoint.Count(s => s.Id < 0);
                int countAllRows = ((GlobalReceivingPoint)DgvReceivingPoint.DataContext).CollectionReceivingPoint.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalReceivingPoint)DgvReceivingPoint.DataContext).CollectionReceivingPoint.RemoveAt(iCount);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((TableReceivingPoint)DgvReceivingPoint.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }

        /// <summary>
        /// Определить имя таблицы
        /// </summary>
        /// <returns></returns>
        private NameTable FindNameTableReceivingPoint()
        {
            switch (NameTable)
            {
                case ReceivingPoint.LocalPoints:
                    return KirasaModelsDBLib.NameTable.TableLocalPoints;
                case ReceivingPoint.RemotePoints:
                    return KirasaModelsDBLib.NameTable.TableRemotePoints;
                default:
                    break;
            }
            return KirasaModelsDBLib.NameTable.TableLocalPoints;
        }

        /// <summary>
        /// Определить тип таблицы
        /// </summary>
        /// <param name="sr"></param>
        /// <returns></returns>
        private AbstractCommonTable FindTypeReceivingPoint(TableReceivingPoint table)
        {
            switch (NameTable)
            {
                case ReceivingPoint.LocalPoints:
                    return table.ToLocalPoint();
                case ReceivingPoint.RemotePoints:
                    return table.ToRemotePoint();
                default:
                    break;
            }
            return null;
        }

        /// <summary>
        /// Установить удаленный пункт
        /// </summary>
        /// <param name="coord"></param>
        public void SetPointToPG(Coord coord)
        {
            try
            {
                if (ReceivingPointWindow != null)
                {
                    ReceivingPointWindow.ReceivingPoint.Coordinates.Latitude = coord.Latitude;
                    ReceivingPointWindow.ReceivingPoint.Coordinates.Longitude = coord.Longitude;
                }
            }
            catch { }
        }
    }
}
