﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ReceivingPointControl
{
    public class GlobalReceivingPoint
    {
        public ObservableCollection<TableReceivingPoint> CollectionReceivingPoint { get; set; }

        public GlobalReceivingPoint()
        {
            try
            {
                CollectionReceivingPoint = new ObservableCollection<TableReceivingPoint> { };

                //CollectionReceivingPoint = new ObservableCollection<TableReceivingPoint>
                //{
                //    new TableReceivingPoint()
                //    {
                //        Id = 1,
                //        Antenna = 200,
                //        IsCetral = true,
                //        Coordinates = new Coord
                //        {
                //            Latitude = 53.7899777,
                //            Longitude = 27.77565,
                //            Altitude = 150
                //        },
                //        Note = "FHFFJGHJGV"
                //    },
                //    new TableReceivingPoint()
                //    {
                //        Id = 2,
                //        Antenna = 160,
                //        IsCetral = false,
                //        Coordinates = new Coord
                //        {
                //            Latitude = 54.643233,
                //            Longitude = 30.746425,
                //            Altitude = 245
                //        },
                //        Note = "HGHHK GHJHJ"
                //    }
                //};
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

      
    }
}
