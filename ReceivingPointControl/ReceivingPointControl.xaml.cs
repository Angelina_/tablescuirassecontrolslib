﻿using KirasaModelsDBLib;
using System;
using System.Windows;
using System.Windows.Controls;
using TableEvents;
using ValuesCorrectLib;

namespace ReceivingPointControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlReceivingPoint : UserControl
    {
        public ReceivingPointProperty ReceivingPointWindow;

        #region Events
        public event EventHandler<TableEvent> OnAddRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };
        public event EventHandler<CenteringEvent> OnCentering = (object sender, CenteringEvent data) => { };
        // Открылось окно с PropertyGrid
        public event EventHandler<ReceivingPointProperty> OnIsWindowPropertyOpen = (object sender, ReceivingPointProperty data) => { };

        public event EventHandler<TableEventReport> OnAddTableToReport = (object sender, TableEventReport data) => { };

        #endregion

        #region Properties
        public ReceivingPoint NameTable { get; set; } = ReceivingPoint.LocalPoints;
        #endregion

        public UserControlReceivingPoint()
        {
            InitializeComponent();

            DgvReceivingPoint.DataContext = new GlobalReceivingPoint();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            ReceivingPointWindow = new ReceivingPointProperty(((GlobalReceivingPoint)DgvReceivingPoint.DataContext).CollectionReceivingPoint);
            ComboBoxEditor.SelectedIndNumPoint = 1;
            OnIsWindowPropertyOpen(this, ReceivingPointWindow);

            //////////////////////////////////////////////////////////////////////////
            if (!PropIsRecReceivingPoint.IsRecAdd && !PropIsRecReceivingPoint.IsRecChange)
            {
                ReceivingPointWindow.Show();
                PropIsRecReceivingPoint.IsRecAdd = true;
                ReceivingPointWindow.OnAddRecordPG += new EventHandler<TableReceivingPoint>(ReceivingPointWindow_OnAddRecordPG);
            }
            //////////////////////////////////////////////////////////////////////////

            //if (ReceivingPointWindow.ShowDialog() == true)
            //{
            //    // Событие добавления одной записи
            //    OnAddRecord(this, new TableEvent(FindTypeReceivingPoint(ReceivingPointWindow.ReceivingPoint)));
            //}
        }

        private void ReceivingPointWindow_OnAddRecordPG(object sender, TableReceivingPoint e)
        {
            PropIsRecReceivingPoint.IsRecAdd = false;
            // Событие добавления одной записи
            OnAddRecord(this, new TableEvent(FindTypeReceivingPoint(e)));

        }

        private void ButtonChange_Click(object sender, RoutedEventArgs e)
        {
            if ((TableReceivingPoint)DgvReceivingPoint.SelectedItem != null)
            {
                if (((TableReceivingPoint)DgvReceivingPoint.SelectedItem).Id > 0)
                {
                    var selected = (TableReceivingPoint)DgvReceivingPoint.SelectedItem;

                    ReceivingPointWindow = new ReceivingPointProperty(((GlobalReceivingPoint)DgvReceivingPoint.DataContext).CollectionReceivingPoint, selected.Clone());

                    OnIsWindowPropertyOpen(this, ReceivingPointWindow);

                    //////////////////////////////////////////////////////////////////////////
                    if (!PropIsRecReceivingPoint.IsRecAdd && !PropIsRecReceivingPoint.IsRecChange)
                    {
                        ReceivingPointWindow.Show();
                        PropIsRecReceivingPoint.IsRecChange = true;
                        ReceivingPointWindow.OnChangeRecordPG += new EventHandler<TableReceivingPoint>(ReceivingPointWindow_OnChangeRecordPG);
                    }
                    //////////////////////////////////////////////////////////////////////////

                    //if (ReceivingPointWindow.ShowDialog() == true)
                    //{
                    //    // Событие изменения одной записи
                    //    OnChangeRecord(this, new TableEvent(FindTypeReceivingPoint(ReceivingPointWindow.ReceivingPoint)));
                    //}
                }
            }
        }

        private void ReceivingPointWindow_OnChangeRecordPG(object sender, TableReceivingPoint e)
        {
            PropIsRecReceivingPoint.IsRecChange = false;
            // Событие изменения одной записи
            OnChangeRecord(this, new TableEvent(FindTypeReceivingPoint(e)));
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableReceivingPoint)DgvReceivingPoint.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TableReceivingPoint tableReceivingPoint = new TableReceivingPoint
                    {
                        Id = ((TableReceivingPoint)DgvReceivingPoint.SelectedItem).Id,
                        Coordinates = new Coord { }
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent(FindTypeReceivingPoint(tableReceivingPoint)));
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, FindNameTableReceivingPoint());
            }
            catch { }
        }

        private void DgvReceivingPoint_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void DgvReceivingPoint_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                if ((TableReceivingPoint)DgvReceivingPoint.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    // Событие Центрирование карты по выбранному пункту 
                    OnCentering(this, new CenteringEvent(((TableReceivingPoint)DgvReceivingPoint.SelectedItem).Coordinates));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }
    }
}
