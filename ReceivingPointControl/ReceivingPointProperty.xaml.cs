﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TableEvents;
using ValuesCorrectLib;

namespace ReceivingPointControl
{
    /// <summary>
    /// Логика взаимодействия для ReceivingPointProperty.xaml
    /// </summary>
    public partial class ReceivingPointProperty : Window
    {
        public event EventHandler<TableReceivingPoint> OnAddRecordPG = (object sender, TableReceivingPoint data) => { };
        public event EventHandler<TableReceivingPoint> OnChangeRecordPG = (object sender, TableReceivingPoint data) => { };

        private ObservableCollection<TableReceivingPoint> collectionTemp;
        public TableReceivingPoint ReceivingPoint { get; private set; }

        public ReceivingPointProperty(ObservableCollection<TableReceivingPoint> collectionReceivingPoint)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionReceivingPoint;
                ReceivingPoint = new TableReceivingPoint();
                propertyGrid.SelectedObject = ReceivingPoint;
                ComboBoxEditor.IsChangeNumPoint = true;

                //propertyGrid.Properties[nameof(TableReceivingPoint.Id)].IsReadOnly = false;

                //Title = "Add record";
                Title = SMeaning.meaningAddRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/AddRec.ico", UriKind.Absolute));

                propertyGrid.Categories.FirstOrDefault(t => t.Name == "Coordinates").HeaderCategoryName = SMeaning.meaningCoord;

                InitProperty();
                //ChangeCategories();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        public ReceivingPointProperty(ObservableCollection<TableReceivingPoint> collectionReceivingPoint, TableReceivingPoint tableReceivingPoint)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionReceivingPoint;
                ReceivingPoint = tableReceivingPoint;
                propertyGrid.SelectedObject = ReceivingPoint;
                ComboBoxEditor.IsChangeNumPoint = false;
                ComboBoxEditor.SelectedIndNumPoint = ReceivingPoint.Id;

                //propertyGrid.Properties[nameof(TableReceivingPoint.Id)].IsReadOnly = true;

                Title = SMeaning.meaningChangeRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/ChangeRec.ico", UriKind.Absolute));

                propertyGrid.Categories.FirstOrDefault(t => t.Name == "Coordinates").HeaderCategoryName = SMeaning.meaningCoord;
                //ChangeCategories();
                InitProperty();
            }
            catch { }
        }

        public ReceivingPointProperty()
        {
            InitializeComponent();

            InitEditors();

            propertyGrid.Categories.FirstOrDefault(t => t.Name == "Coordinates").HeaderCategoryName = SMeaning.meaningCoord;
            
            //ChangeCategories();
            InitProperty();

        }

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IsAddClick((TableReceivingPoint)propertyGrid.SelectedObject) != null)
                {
                    if (PropIsRecReceivingPoint.IsRecAdd)
                    {
                        OnAddRecordPG?.Invoke(sender, (TableReceivingPoint)propertyGrid.SelectedObject);
                    }

                    if (PropIsRecReceivingPoint.IsRecChange)
                    {
                        OnChangeRecordPG?.Invoke(sender, (TableReceivingPoint)propertyGrid.SelectedObject);
                    }

                    Close();

                    //DialogResult = true;
                }
            }
            catch { }
        }

        public TableReceivingPoint IsAddClick(TableReceivingPoint ReceivingPointWindow)
        {
            CorrectParams.IsCorrectAntenna(ReceivingPointWindow);

            ReceivingPointWindow.Id = ComboBoxEditor.SelectedIndNumPoint;
            if (ComboBoxEditor.IsChangeNumPoint)
            {
                if (CorrectParams.IsAddReceivingPoint(collectionTemp, ReceivingPointWindow))
                {
                    if (ReceivingPointWindow.Id == 1)
                    {
                        ReceivingPointWindow.IsCetral = true;
                    }

                    return ReceivingPointWindow;
                }
            }
            else return ReceivingPointWindow;

            return null;
        }

        private void ButtonNoApply_Click(object sender, RoutedEventArgs e)
        {
            Close();
            PropIsRecReceivingPoint.IsRecAdd = false;
            PropIsRecReceivingPoint.IsRecChange = false;

            //DialogResult = false;
        }

        private void InitEditors()
        {
            propertyGrid.Editors.Add(new ReceivingPointIdEditor(nameof(ReceivingPoint.Id), typeof(TableReceivingPoint)));
            propertyGrid.Editors.Add(new ReceivingPointCoordinatesEditor(nameof(ReceivingPoint.Coordinates), typeof(TableReceivingPoint)));
            propertyGrid.Editors.Add(new ReceivingPointAntennaEditor(nameof(ReceivingPoint.Antenna), typeof(TableReceivingPoint)));
            propertyGrid.Editors.Add(new ReceivingPointNoteEditor(nameof(ReceivingPoint.Note), typeof(TableReceivingPoint)));
            propertyGrid.Editors.Add(new ReceivingPointErrorEditor(nameof(ReceivingPoint.TimeError), typeof(TableReceivingPoint)));
        }

        private void InitProperty()
        {
            foreach (var property in propertyGrid.Properties)
            {
                try
                {
                    if (property.IsBrowsable == false) { continue; }

                    if (property.PropertyValue.SubProperties.Count != 0)
                    {
                        foreach (var subProperty in property.PropertyValue.SubProperties)
                            subProperty.PropertyValue.PropertyValueException += PropertyGridSubException;
                        continue;
                    }
                    property.PropertyValue.PropertyValueException += PropertyGridException;
                }
                catch (Exception ex)
                { System.Windows.MessageBox.Show(ex.Message); }
            }
        }

        private void PropertyGridSubException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void PropertyGridException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void gridProperty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                if (IsAddClick((TableReceivingPoint)propertyGrid.SelectedObject) != null)
                {
                    DialogResult = true;
                }
            }
        }

        public void SetLanguagePropertyGrid(DllCuirassemProperties.Models.Languages language)
        {
            LoadTranslatorPropertyGrid(language);
            TranslatorTables.ChangeLanguagePropertyGrid(language, propertyGrid);
        }

        private void LoadTranslatorPropertyGrid(DllCuirassemProperties.Models.Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case DllCuirassemProperties.Models.Languages.EN:
                        dict.Source = new Uri("/Cuirasse;component/Languages/TranslatorTables/TranslatorTablesCuirasse.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case DllCuirassemProperties.Models.Languages.RU:
                        dict.Source = new Uri("/Cuirasse;component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/Cuirasse;component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                // TEST------------------------------------------------------------------------------------------------------
                //switch (language)
                //{
                //    case DllCuirassemProperties.Models.Languages.EN:
                //        dict.Source = new Uri("/ProjectTablesControlTEST;component/Languages/TranslatorTables/TranslatorTablesCuirasse.EN.xaml",
                //                      UriKind.Relative);
                //        break;
                //    case DllCuirassemProperties.Models.Languages.RU:
                //        dict.Source = new Uri("/ProjectTablesControlTEST;component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                //                            UriKind.Relative);
                //        break;
                //    default:
                //        dict.Source = new Uri("/ProjectTablesControlTEST;component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                //                          UriKind.Relative);
                //        break;
                //}
                // ------------------------------------------------------------------------------------------------------ TEST

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            PropIsRecReceivingPoint.IsRecAdd = false;
            PropIsRecReceivingPoint.IsRecChange = false;
        }
    }
}
