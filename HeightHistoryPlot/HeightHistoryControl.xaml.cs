﻿using HeightHistoryPlot.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HeightHistoryPlot
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class HeightHistoryControl : UserControl
    {
        public static HeightHistoryControl Instance;

        private readonly Point hlabelOffset = new Point(23, 7);
        private List<float> collectionHistory;

        #region default values
        private SmoothingMode axisSmoothingMode = SmoothingMode.HighQuality;
        private SmoothingMode plotSmoothingMode = SmoothingMode.HighSpeed;
        private System.Windows.Thickness plotBorderSpacing = new System.Windows.Thickness(0.15, 0.05, 0.1, 0.05);
        private Color axisPenColor = Color.White;
        private Color altitudesPlotPenColor = Color.FromArgb(255, 0, 215, 249);
        private Color gridColor = Color.FromArgb(128, 128, 128, 128);
        private Font drawStringFont = new Font("Cambria", 8);
        private SolidBrush brushString = new SolidBrush(Color.White);

        private int historyPointsLimit = 15;
        private int gridRows = 6;
        private int gridColumns = 6;

        private float axisPenThickness = 1;
        private float altitudesPlotPenThickness = 1;
        private float gridThickness = 0.5f;
        private float hcoefficient = 1;
        private float maximumHeightScope = 0;
        private float minimumHeightScope = 0;

        private bool isMainFrameEnabled = true;
        private bool isCollectionLimited = true;
        private bool isGridRowsEnabled = true;
        private bool isGridColumnsEnabled = true;
        private bool isLabelingEnabled = true;
        #endregion

        #region Event Delegates
        public delegate void CollectionHandler(List<float> collection);
        #endregion

        #region Events
        public event CollectionHandler CollectionChanged;
        #endregion

        public HeightHistoryControl()
        {
            InitializeComponent();

            this.CollectionChanged += HeightHistoryControl_CollectionChanged;
        }

        #region Properties

        public static readonly System.Windows.DependencyProperty CollectionHistoryProperty = 
            System.Windows.DependencyProperty.Register(
                "CollectionHistorySource", 
                typeof(IEnumerable), 
                typeof(HeightHistoryControl), 
                new System.Windows.PropertyMetadata(null, new System.Windows.PropertyChangedCallback(OnCollectionHistoryChanged))
            );

        public List CollectionHistorySource
        {
            get => (List)GetValue(CollectionHistoryProperty);
            set => SetValue(CollectionHistoryProperty, value);
        }

        public System.Windows.Thickness PlotBorderSpacingCoefficients
        {
            get => this.plotBorderSpacing;
            set => this.plotBorderSpacing = value;
        }

        public int HistoryPointsLimit
        {
            get => this.historyPointsLimit;
            set
            {
                if (value > 1)
                    this.historyPointsLimit = value;
            }
        }

        public int GridRows
        {
            get => this.gridRows;
            set => this.gridRows = value;
        }

        public int GridColumns
        {
            get => this.gridColumns;
            set => this.gridColumns = value;
        }

        public SmoothingMode PlotSmoothingMode
        {
            get => this.plotSmoothingMode;
            set => this.plotSmoothingMode = value;
        }

        public SmoothingMode AxisSmoothingMode
        {
            get => this.axisSmoothingMode;
            set => this.axisSmoothingMode = value;
        }

        public Color AxisPenColor
        {
            get => this.axisPenColor;
            set => this.axisPenColor = value;
        }

        public Color AltitudesPlotPenColor
        {
            get => this.altitudesPlotPenColor;
            set => this.altitudesPlotPenColor = value;
        }

        public Color GridColor
        {
            get => this.gridColor;
            set => this.gridColor = value;
        }

        public Font DrawStringFont
        {
            get => this.drawStringFont;
            set => this.drawStringFont = value;
        }

        public SolidBrush BrushString
        {
            get => this.brushString;
            set => this.brushString = value;
        }

        public float AxisPenThickness
        {
            get => this.axisPenThickness;
            set
            {
                if (value > 0)
                    this.axisPenThickness = value;
            }
        }

        public float AltitudesPlotPenThickness
        {
            get => this.altitudesPlotPenThickness;
            set
            {
                if (value > 0)
                    this.altitudesPlotPenThickness = value;
            }
        }

        public float GridThickness
        {
            get => this.gridThickness;
            set
            {
                if (value > 0)
                    this.gridThickness = value;
            }
        }

        public bool IsMainFrameEnabled
        {
            get => this.isMainFrameEnabled;
            set => this.isMainFrameEnabled = value;
        }

        public bool IsCollectionLimited
        {
            get => this.isCollectionLimited;
            set => this.isCollectionLimited = value;
        }

        public bool IsGridRowsEnabled
        {
            get => this.isGridRowsEnabled;
            set => this.isGridRowsEnabled = value;
        }

        public bool IsGridColumnsEnabled
        {
            get => this.isGridColumnsEnabled;
            set => this.isGridColumnsEnabled = value;
        }

        public bool IsLabelingEnabled
        {
            get => this.isLabelingEnabled;
            set => this.isLabelingEnabled = value;
        }
        #endregion

        #region EventHandlers

        private static void OnCollectionHistoryChanged(System.Windows.DependencyObject d, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            Instance = d as HeightHistoryControl;
            var items = (List<float>)e.NewValue;

            if (items != null)
                Instance.CollectionChanged.Invoke(items);
        }

        private void HeightHistoryControl_CollectionChanged(List<float> collection)
        {
            if (this.collectionHistory != null)
                this.collectionHistory.Clear();

            this.collectionHistory = collection;

            if (this.IsLoaded)
            {
                this.FindCollectionMaximum();
                this.FindCollectionMinimum();
                this.Render();
            }
        }

        #endregion

        private void Render()
        {
            using (var bitmap = new Bitmap((int)this.Graph1_Canvas.ActualWidth, (int)(this.Graph1_Canvas.ActualHeight)))
            using (var graphics = Graphics.FromImage(bitmap))
            {
                CanvasQuadrant quadrantFrame = new CanvasQuadrant()
                {
                    LowerXPoint = (float)(this.plotBorderSpacing.Left * bitmap.Width),
                    UpperXPoint = (float)((1 - this.plotBorderSpacing.Right) * bitmap.Width),
                    LowerYPoint = (float)(this.plotBorderSpacing.Top * bitmap.Height),
                    UpperYPoint = (float)((1 - this.plotBorderSpacing.Bottom) * bitmap.Height)
                };

                if (this.IsCollectionLimited && this.collectionHistory.Count > HistoryPointsLimit)
                    this.collectionHistory = this.collectionHistory.Skip(this.collectionHistory.Count - HistoryPointsLimit).ToList();

                this.CalculateHCoefficient(quadrantFrame);

                if (this.IsMainFrameEnabled)
                    this.DrawMainFrame(graphics, quadrantFrame);

                if (this.collectionHistory.Count > 1)
                    this.DrawMainPlot(graphics, quadrantFrame);

                this.DrawGrid(graphics, quadrantFrame);

                if (this.IsLabelingEnabled)
                    this.DrawLabels(graphics, quadrantFrame);

                this.Image_Graph1.Source = this.BmpImageFromBmp(bitmap);
            }
        }

        private void DrawMainFrame(Graphics graphics, CanvasQuadrant quadrant)
        {
            graphics.SmoothingMode = this.AxisSmoothingMode;

            using (var pen = new Pen(this.AxisPenColor, this.AxisPenThickness))
            {
                this.DrawLeftAxis(graphics, quadrant, pen);
                this.DrawTopAxis(graphics, quadrant, pen);
                this.DrawRightAxis(graphics, quadrant, pen);
                this.DrawBottomAxis(graphics, quadrant, pen);
            }
        }
        
        private void DrawLeftAxis(Graphics graphics, CanvasQuadrant quadrant, Pen pen)
        {
            graphics.DrawLine(
                pen,
                quadrant.LowerXPoint,
                quadrant.LowerYPoint,
                quadrant.LowerXPoint,
                quadrant.UpperYPoint
            );
        }
        
        private void DrawTopAxis(Graphics graphics, CanvasQuadrant quadrant, Pen pen)
        {
            graphics.DrawLine(
                pen,
                quadrant.LowerXPoint,
                quadrant.LowerYPoint,
                quadrant.UpperXPoint,
                quadrant.LowerYPoint
            );
        }
        
        private void DrawRightAxis(Graphics graphics, CanvasQuadrant quadrant, Pen pen)
        {
            graphics.DrawLine(
                pen,
                quadrant.UpperXPoint,
                quadrant.LowerYPoint,
                quadrant.UpperXPoint,
                quadrant.UpperYPoint
            );
        }
        
        private void DrawBottomAxis(Graphics graphics, CanvasQuadrant quadrant, Pen pen)
        {
            graphics.DrawLine(
                pen,
                quadrant.UpperXPoint,
                quadrant.UpperYPoint,
                quadrant.LowerXPoint,
                quadrant.UpperYPoint
            );
        }

        private void DrawMainPlot(Graphics graphics, CanvasQuadrant quadrant)
        {
            graphics.SmoothingMode = this.PlotSmoothingMode;

            using (var pen = new Pen(this.altitudesPlotPenColor, this.altitudesPlotPenThickness))
            {
                float stepXDenominator = this.CalculateStepXDenominator();
                float stepX = (quadrant.UpperXPoint - quadrant.LowerXPoint) / stepXDenominator;

                for (int i = 1; i < this.collectionHistory.Count; i++)
                {
                    float x1 = quadrant.LowerXPoint + (i - 1) * stepX;
                    float y1 = quadrant.UpperYPoint - (this.collectionHistory[i - 1] - this.minimumHeightScope) * this.hcoefficient;

                    float x2 = quadrant.LowerXPoint + i * stepX;
                    float y2 = quadrant.UpperYPoint - (this.collectionHistory[i] - this.minimumHeightScope) * this.hcoefficient;

                    graphics.DrawLine(
                        pen,
                        x1,
                        y1,
                        x2,
                        y2
                    );
                }
            }
        }

        private void DrawGrid(Graphics graphics, CanvasQuadrant quadrant)
        {
            using (var pen = new Pen(this.GridColor, this.GridThickness))
            {
                pen.DashStyle = DashStyle.Dash;

                if (this.IsGridRowsEnabled)
                    this.DrawGridRows(graphics, pen, quadrant);

                if (this.IsGridColumnsEnabled)
                    this.DrawGridColumns(graphics, pen, quadrant);
            }
        }

        private void DrawGridRows(Graphics graphics, Pen pen, CanvasQuadrant quadrant)
        {
            float rowWidth = (quadrant.UpperYPoint - quadrant.LowerYPoint) / this.GridRows;

            for (int i = 1; i < this.GridRows; i++)
            {
                float x1 = quadrant.LowerXPoint;
                float y1 = quadrant.LowerYPoint + i * rowWidth;

                float x2 = quadrant.UpperXPoint;
                float y2 = y1;

                graphics.DrawLine(
                    pen,
                    x1,
                    y1,
                    x2,
                    y2
                );

                if (this.collectionHistory.Count == 0)
                    continue;

                float height = (this.GridRows - i) * rowWidth / this.hcoefficient + this.minimumHeightScope;

                if (IsLabelingEnabled)
                    this.DrawLabelHeight(
                        graphics,
                        height,
                        new FloatPoint(quadrant.LowerXPoint, y1)
                    );
            }
        }

        private void DrawGridColumns(Graphics graphics, Pen pen, CanvasQuadrant quadrant)
        {
            float columnWidth = (quadrant.UpperXPoint - quadrant.LowerXPoint) / this.GridColumns;

            for (int i = 1; i < this.GridColumns; i++)
            {
                float x1 = quadrant.LowerXPoint + i * columnWidth;
                float y1 = quadrant.LowerYPoint;

                float x2 = x1;
                float y2 = quadrant.UpperYPoint;

                graphics.DrawLine(
                    pen,
                    x1,
                    y1,
                    x2,
                    y2
                );
            }
        }

        private void DrawLabels(Graphics graphics, CanvasQuadrant quadrant)
        {
            // Y axis label.
            this.DrawLabel(graphics, "H,m", new FloatPoint(quadrant.LowerXPoint, quadrant.LowerYPoint));
        }

        private void DrawLabel(Graphics graphics, string label, FloatPoint point)
        {
            point.X -= this.hlabelOffset.X;
            point.Y -= this.hlabelOffset.Y;

            graphics.DrawString(
                label, 
                this.DrawStringFont, 
                this.BrushString, 
                point.X, 
                point.Y
            );
        }

        private void DrawLabelHeight(Graphics graphics, float height, FloatPoint point)
        {
            var offset = GetOffset(height, out string label);

            point.X -= offset.X;
            point.Y -= offset.Y;

            graphics.DrawString(
                label, 
                this.DrawStringFont, 
                this.BrushString, 
                point.X, 
                point.Y
            );
        }

        private Point GetOffset(float height, out string label)
        {           
            int xoffset = 0;

            float heightDifference = this.maximumHeightScope - this.minimumHeightScope;
            bool isHeightDifferenceLowerOrEqualRowsAmount = heightDifference == 0 ? false : heightDifference <= this.GridRows;
            label = height.ToString(isHeightDifferenceLowerOrEqualRowsAmount ? "0.0" : "0");

            switch (height.ToString("f0").Length)
            {
                case 0:
                    xoffset = 0;
                    break;
                case 1 when !isHeightDifferenceLowerOrEqualRowsAmount:
                    xoffset = 11;
                    break;
                case 1 when isHeightDifferenceLowerOrEqualRowsAmount:
                    xoffset = 17;
                    break;
                case 2:
                    xoffset = 18;
                    break;
                case 3:
                    xoffset = 23;
                    break;
                default:
                    label = string.Format("{0}.{1}e{2}", label[0], label[1], label.Length - 1);
                    xoffset = 28;
                    break;
            }

            return new Point(xoffset, 7);
        }

        private BitmapImage BmpImageFromBmp(Bitmap bitmap)
        {
            using (var memory = new System.IO.MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Png);
                memory.Position = 0;

                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
                bitmapImage.Freeze();

                return bitmapImage;
            }
        }

        private void FindCollectionMaximum()
        {
            if (this.collectionHistory.Count != 0)
                this.maximumHeightScope = this.collectionHistory.Max();
        }

        private void FindCollectionMinimum()
        {
            if (this.collectionHistory.Count != 0)
                this.minimumHeightScope = this.collectionHistory.Min();
        }

        private void CalculateHCoefficient(CanvasQuadrant quadrant)
        {
            try
            {
                this.hcoefficient = (quadrant.UpperYPoint - quadrant.LowerYPoint) / (this.maximumHeightScope - this.minimumHeightScope);
            }
            catch (DivideByZeroException)
            {
                this.hcoefficient = 1;
            }
        }

        private float CalculateStepXDenominator()
        {
            float stepXDenominator = 0;

            if (isCollectionLimited && this.collectionHistory.Count > this.HistoryPointsLimit)
                stepXDenominator = this.HistoryPointsLimit - 1;
            else
                stepXDenominator = this.collectionHistory.Count - 1;

            return stepXDenominator;
        }
    }
}
