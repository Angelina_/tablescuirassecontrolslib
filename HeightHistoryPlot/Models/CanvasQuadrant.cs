﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeightHistoryPlot.Models
{
    internal class CanvasQuadrant
    {
        public float LowerXPoint { get; set; }

        public float UpperXPoint { get; set; }

        public float LowerYPoint { get; set; }

        public float UpperYPoint { get; set; }
    }
}
