﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeightHistoryPlot.Models
{
    public struct FloatPoint
    {
        public float X { get; set; }
        
        public float Y { get; set; }

        public FloatPoint(float x, float y)
        {
            this.X = x;
            this.Y = y;
        }
    }
}
