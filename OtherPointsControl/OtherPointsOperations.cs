﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace OtherPointsControl
{
    public partial class UserControlOtherPoints : UserControl
    {
        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="listOtherPoints"></param>
        public void UpdateOtherPoints(List<TableOtherPoints> listOtherPoints)
        {
            try
            {
                if (listOtherPoints == null)
                    return;

                ((GlobalOtherPoints)DgvOtherPoints.DataContext).CollectionOtherPoints.Clear();

                for (int i = 0; i < listOtherPoints.Count; i++)
                {
                    ((GlobalOtherPoints)DgvOtherPoints.DataContext).CollectionOtherPoints.Add(listOtherPoints[i]);
                }

                AddEmptyRows();

            }
            catch { }
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listOtherPoints"></param>
        public void AddOtherPoints(List<TableOtherPoints> listOtherPoints)
        {
            try
            {
                DeleteEmptyRows();

                for (int i = 0; i < listOtherPoints.Count; i++)
                {
                    int ind = ((GlobalOtherPoints)DgvOtherPoints.DataContext).CollectionOtherPoints.ToList().FindIndex(x => x.Id == listOtherPoints[i].Id);
                    if (ind != -1)
                    {
                        ((GlobalOtherPoints)DgvOtherPoints.DataContext).CollectionOtherPoints[ind] = listOtherPoints[i];
                    }
                    else
                    {
                        ((GlobalOtherPoints)DgvOtherPoints.DataContext).CollectionOtherPoints.Add(listOtherPoints[i]);
                    }
                }

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteOtherPoints(TableOtherPoints tableOtherPoints)
        {
            try
            {
                int index = ((GlobalOtherPoints)DgvOtherPoints.DataContext).CollectionOtherPoints.ToList().FindIndex(x => x.Id == tableOtherPoints.Id);
                ((GlobalOtherPoints)DgvOtherPoints.DataContext).CollectionOtherPoints.RemoveAt(index);

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearOtherPoints()
        {
            try
            {
                ((GlobalOtherPoints)DgvOtherPoints.DataContext).CollectionOtherPoints.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvOtherPoints.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvOtherPoints.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvOtherPoints.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalOtherPoints)DgvOtherPoints.DataContext).CollectionOtherPoints.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalOtherPoints)DgvOtherPoints.DataContext).CollectionOtherPoints.RemoveAt(index);
                    }
                }

                List<TableOtherPoints> list = new List<TableOtherPoints>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    TableOtherPoints strOP = new TableOtherPoints
                    {
                        Id = -2,
                        Type = (TypeOtherPoints)255,
                        Coordinates = new Coord() { Altitude = -2, Latitude = -2, Longitude = -2 }
                    };
                    list.Add(strOP);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalOtherPoints)DgvOtherPoints.DataContext).CollectionOtherPoints.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalOtherPoints)DgvOtherPoints.DataContext).CollectionOtherPoints.Count(s => s.Id < 0);
                int countAllRows = ((GlobalOtherPoints)DgvOtherPoints.DataContext).CollectionOtherPoints.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalOtherPoints)DgvOtherPoints.DataContext).CollectionOtherPoints.RemoveAt(iCount);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((TableOtherPoints)DgvOtherPoints.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }

    }
}
