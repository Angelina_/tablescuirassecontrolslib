﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TableEvents;

namespace OtherPointsControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlOtherPoints : UserControl
    {
        public OtherPointsProperty OtherPointsWindow;

        #region Events
        public event EventHandler<TableEvent> OnAddRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };
        public event EventHandler<CenteringEvent> OnCentering = (object sender, CenteringEvent data) => { };
        // Открылось окно с PropertyGrid
        public event EventHandler<OtherPointsProperty> OnIsWindowPropertyOpen = (object sender, OtherPointsProperty data) => { };

        public event EventHandler<TableEventReport> OnAddTableToReport = (object sender, TableEventReport data) => { };

        #endregion

        public UserControlOtherPoints()
        {
            InitializeComponent();

            DgvOtherPoints.DataContext = new GlobalOtherPoints();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OtherPointsWindow = new OtherPointsProperty(((GlobalOtherPoints)DgvOtherPoints.DataContext).CollectionOtherPoints);
              
                OnIsWindowPropertyOpen(this, OtherPointsWindow);

                if (!PropIsRecOtherPoints.IsRecAdd && !PropIsRecOtherPoints.IsRecChange)
                {
                    OtherPointsWindow.Show();
                    PropIsRecOtherPoints.IsRecAdd = true;
                    OtherPointsWindow.OnAddRecordPG += new EventHandler<TableOtherPoints>(OtherPointsWindow_OnAddRecordPG);
                }
            }
            catch { }
            //catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void OtherPointsWindow_OnAddRecordPG(object sender, TableOtherPoints e)
        {
            PropIsRecOtherPoints.IsRecAdd = false;
            // Событие добавления одной записи
            OnAddRecord(this, new TableEvent(e));

        }

        private void ButtonChange_Click(object sender, RoutedEventArgs e)
        {
            if ((TableOtherPoints)DgvOtherPoints.SelectedItem != null)
            {
                if (((TableOtherPoints)DgvOtherPoints.SelectedItem).Id > 0)
                {
                    var selected = (TableOtherPoints)DgvOtherPoints.SelectedItem;

                    OtherPointsWindow = new OtherPointsProperty(((GlobalOtherPoints)DgvOtherPoints.DataContext).CollectionOtherPoints, selected.Clone());

                    OnIsWindowPropertyOpen(this, OtherPointsWindow);

                    if (!PropIsRecOtherPoints.IsRecAdd && !PropIsRecOtherPoints.IsRecChange)
                    {
                        OtherPointsWindow.Show();
                        PropIsRecOtherPoints.IsRecChange = true;
                        OtherPointsWindow.OnChangeRecordPG += new EventHandler<TableOtherPoints>(OtherPointsWindow_OnChangeRecordPG);
                    }
                }
            }
        }

        private void OtherPointsWindow_OnChangeRecordPG(object sender, TableOtherPoints e)
        {
            PropIsRecOtherPoints.IsRecChange = false;
            // Событие изменения одной записи
            OnChangeRecord(this, new TableEvent(e));
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableOtherPoints)DgvOtherPoints.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TableOtherPoints tableOtherPoints = new TableOtherPoints
                    {
                        Id = ((TableOtherPoints)DgvOtherPoints.SelectedItem).Id,
                        Coordinates = new Coord { }
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent(tableOtherPoints));
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            // Событие удаления записей
            OnClearRecords(this, NameTable.TableOtherPoints);
        }

        private void DgvOtherPoints_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void DgvOtherPoints_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }
    }
}
