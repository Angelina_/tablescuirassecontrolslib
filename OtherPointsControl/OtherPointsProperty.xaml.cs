﻿using KirasaModelsDBLib;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using TableEvents;
using ValuesCorrectLib;

namespace OtherPointsControl
{
    /// <summary>
    /// Логика взаимодействия для OtherPointsProperty.xaml
    /// </summary>
    public partial class OtherPointsProperty : Window
    {
        public event EventHandler<TableOtherPoints> OnAddRecordPG = (object sender, TableOtherPoints data) => { };
        public event EventHandler<TableOtherPoints> OnChangeRecordPG = (object sender, TableOtherPoints data) => { };

        private ObservableCollection<TableOtherPoints> collectionTemp;
        public TableOtherPoints tableOtherPoints { get; private set; }

        public OtherPointsProperty(ObservableCollection<TableOtherPoints> collectionOtherPoints)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionOtherPoints;
                tableOtherPoints = new TableOtherPoints();
                if (tableOtherPoints.Coordinates.Latitude != -1)
                {
                    tableOtherPoints.Coordinates.Latitude = tableOtherPoints.Coordinates.Latitude < 0 ? tableOtherPoints.Coordinates.Latitude * -1 : tableOtherPoints.Coordinates.Latitude;
                    tableOtherPoints.Coordinates.Latitude = Math.Round(tableOtherPoints.Coordinates.Latitude, 6);
                }
                if (tableOtherPoints.Coordinates.Longitude != -1)
                {
                    tableOtherPoints.Coordinates.Longitude = tableOtherPoints.Coordinates.Longitude < 0 ? tableOtherPoints.Coordinates.Longitude * -1 : tableOtherPoints.Coordinates.Longitude;
                    tableOtherPoints.Coordinates.Longitude = Math.Round(tableOtherPoints.Coordinates.Longitude, 6);
                }
                propertyGrid.SelectedObject = tableOtherPoints;
                propertyGrid.Properties[nameof(TableOtherPoints.Id)].IsReadOnly = false;

                Title = SMeaning.meaningAddRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/AddRec.ico", UriKind.Absolute));

                InitProperty();
                propertyGrid.Categories.FirstOrDefault(t => t.Name == "Coordinates").HeaderCategoryName = SMeaning.meaningCoord;
                //ChangeCategories();

            }
            catch { }
        }

        public OtherPointsProperty(ObservableCollection<TableOtherPoints> collectionOtherPoints, TableOtherPoints otherPoints)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionOtherPoints;
                tableOtherPoints = otherPoints;
                if (tableOtherPoints.Coordinates.Latitude != -1)
                {
                    tableOtherPoints.Coordinates.Latitude = tableOtherPoints.Coordinates.Latitude < 0 ? tableOtherPoints.Coordinates.Latitude * -1 : tableOtherPoints.Coordinates.Latitude;
                    tableOtherPoints.Coordinates.Latitude = Math.Round(tableOtherPoints.Coordinates.Latitude, 6);
                }
                if (tableOtherPoints.Coordinates.Longitude != -1)
                {
                    tableOtherPoints.Coordinates.Longitude = tableOtherPoints.Coordinates.Longitude < 0 ? tableOtherPoints.Coordinates.Longitude * -1 : tableOtherPoints.Coordinates.Longitude;
                    tableOtherPoints.Coordinates.Longitude = Math.Round(tableOtherPoints.Coordinates.Longitude, 6);
                }
                propertyGrid.SelectedObject = tableOtherPoints;
                propertyGrid.Properties[nameof(TableOtherPoints.Id)].IsReadOnly = true;

                Title = SMeaning.meaningChangeRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/ChangeRec.ico", UriKind.Absolute));

                InitProperty();
                propertyGrid.Categories.FirstOrDefault(t => t.Name == "Coordinates").HeaderCategoryName = SMeaning.meaningCoord;
                //ChangeCategories();
            }
            catch { }
        }

        public OtherPointsProperty()
        {
            try
            {
                InitializeComponent();

                InitEditors();
                //ChangeCategories();
                propertyGrid.Categories.FirstOrDefault(t => t.Name == "Coordinates").HeaderCategoryName = SMeaning.meaningCoord;
                InitProperty();
            }

            catch { }
        }

        private void InitProperty()
        {
            foreach (var property in propertyGrid.Properties)
            {
                try
                {
                    if (property.IsBrowsable == false)
                        continue;
                    if (property.PropertyValue.SubProperties.Count != 0)
                    {
                        foreach (var subProperty in property.PropertyValue.SubProperties)
                            subProperty.PropertyValue.PropertyValueException += PropertyGridSubException;
                        continue;
                    }
                    property.PropertyValue.PropertyValueException += PropertyGridException;
                }
                catch (Exception ex)
                { MessageBox.Show(ex.Message); }
            }
        }

        private void PropertyGridSubException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void PropertyGridException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IsAddClick((TableOtherPoints)propertyGrid.SelectedObject) != null)
                {
                    if (PropIsRecOtherPoints.IsRecAdd)
                    {
                        OnAddRecordPG?.Invoke(sender, (TableOtherPoints)propertyGrid.SelectedObject);
                    }

                    if (PropIsRecOtherPoints.IsRecChange)
                    {
                        OnChangeRecordPG?.Invoke(sender, (TableOtherPoints)propertyGrid.SelectedObject);
                    }

                    Close();

                    //DialogResult = true;
                }
            }
            catch { }
        }

        public TableOtherPoints IsAddClick(TableOtherPoints OtherPointsWindow)
        {
            return OtherPointsWindow;
        }

        private void ButtonNoApply_Click(object sender, RoutedEventArgs e)
        {
            Close();

            PropIsRecOtherPoints.IsRecAdd = false;
            PropIsRecOtherPoints.IsRecChange = false;
        }

        private void InitEditors()
        {
            propertyGrid.Editors.Add(new OtherPointsCoordinatesEditor(nameof(tableOtherPoints.Coordinates), typeof(TableOtherPoints)));
            propertyGrid.Editors.Add(new OtherPointsTypeEditor(nameof(tableOtherPoints.Type), typeof(TableOtherPoints)));
        }

        private void gridProperty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                if (IsAddClick((TableOtherPoints)propertyGrid.SelectedObject) != null)
                {
                    DialogResult = true;
                }
            }
        }

        public void SetLanguagePropertyGrid(DllCuirassemProperties.Models.Languages language)
        {
            LoadTranslatorPropertyGrid(language);
            TranslatorTables.ChangeLanguagePropertyGrid(language, propertyGrid);
        }

        private void LoadTranslatorPropertyGrid(DllCuirassemProperties.Models.Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case DllCuirassemProperties.Models.Languages.EN:
                        dict.Source = new Uri("/Cuirasse;component/Languages/TranslatorTables/TranslatorTablesCuirasse.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case DllCuirassemProperties.Models.Languages.RU:
                        dict.Source = new Uri("/Cuirasse;component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/Cuirasse;component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                // TEST ------------------------------------------------------------------------------------------------------
                //switch (language)
                //{
                //    case DllCuirassemProperties.Models.Languages.EN:
                //        dict.Source = new Uri("/ProjectTablesControlTEST;component/Languages/TranslatorTables/TranslatorTablesCuirasse.EN.xaml",
                //                      UriKind.Relative);
                //        break;
                //    case DllCuirassemProperties.Models.Languages.RU:
                //        dict.Source = new Uri("/ProjectTablesControlTEST;component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                //                            UriKind.Relative);
                //        break;
                //    default:
                //        dict.Source = new Uri("/ProjectTablesControlTEST;component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                //                          UriKind.Relative);
                //        break;
                //}
                // ------------------------------------------------------------------------------------------------------ TEST

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            PropIsRecOtherPoints.IsRecAdd = false;
            PropIsRecOtherPoints.IsRecChange = false;
        }
    }
}
