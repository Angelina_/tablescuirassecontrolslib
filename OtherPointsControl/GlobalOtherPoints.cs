﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OtherPointsControl
{
    public class GlobalOtherPoints
    {
        public ObservableCollection<TableOtherPoints> CollectionOtherPoints { get; set; }

        public GlobalOtherPoints()
        {
            try
            {
                CollectionOtherPoints = new ObservableCollection<TableOtherPoints> { };

                //CollectionOtherPoints = new ObservableCollection<TableOtherPoints>
                //{
                //    new TableOtherPoints()
                //    {
                //        Id = 1,
                //        Type = TypeOtherPoints.GrozaS,
                //        Coordinates = new Coord
                //        {
                //            Latitude = 53.7899777,
                //            Longitude = 27.77565,
                //            Altitude = 150
                //        }
                //    },
                //    new TableOtherPoints()
                //    {
                //        Id = 2,
                //        Type = TypeOtherPoints.GrozaS,
                //        Coordinates = new Coord
                //        {
                //            Latitude = 54.643233,
                //            Longitude = 30.746425,
                //            Altitude = 245
                //        }
                //    }
                //};
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
