﻿using KirasaModelsDBLib;
using System;
using System.Windows;
using System.Windows.Controls;
using TableEvents;

namespace FreqRangesControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlFreqRanges : UserControl
    {
        public FreqRangesProperty FreqRangesWindow;

        #region Events
        public event EventHandler<TableEvent> OnAddRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };

        // Открылось окно с PropertyGrid
        public event EventHandler<FreqRangesProperty> OnIsWindowPropertyOpen = (object sender, FreqRangesProperty data) => { };

        public event EventHandler<TableEventReport> OnAddTableToReport = (object sender, TableEventReport data) => { };
        #endregion

        #region Properties
        public FreqRanges NameTable { get; set; } = FreqRanges.FreqKnown;
        #endregion

        public UserControlFreqRanges()
        {
            InitializeComponent();

            DgvFreqRanges.DataContext = new GlobalFreqRanges();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FreqRangesWindow = new FreqRangesProperty(((GlobalFreqRanges)DgvFreqRanges.DataContext).CollectionFreqRanges);

                OnIsWindowPropertyOpen(this, FreqRangesWindow);

                if (FreqRangesWindow.ShowDialog() == true)
                {
                    FreqRangesWindow.FreqRanges.FreqMinKHz = FreqRangesWindow.FreqRanges.FreqMinKHz * 1000;
                    FreqRangesWindow.FreqRanges.FreqMaxKHz = FreqRangesWindow.FreqRanges.FreqMaxKHz * 1000;

                    // Событие добавления одной записи
                    OnAddRecord(this, new TableEvent(FindTypeFreqRanges(FreqRangesWindow.FreqRanges)));
                }
            }
            catch { }
        }

        private void ButtonChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if ((TableFreqRanges)DgvFreqRanges.SelectedItem != null)
                {
                    if (((TableFreqRanges)DgvFreqRanges.SelectedItem).Id > 0)
                    {
                        var selected = (TableFreqRanges)DgvFreqRanges.SelectedItem;

                        FreqRangesWindow = new FreqRangesProperty(((GlobalFreqRanges)DgvFreqRanges.DataContext).CollectionFreqRanges, selected.Clone());

                        OnIsWindowPropertyOpen(this, FreqRangesWindow);

                        if (FreqRangesWindow.ShowDialog() == true)
                        {
                            FreqRangesWindow.FreqRanges.FreqMinKHz = FreqRangesWindow.FreqRanges.FreqMinKHz * 1000;
                            FreqRangesWindow.FreqRanges.FreqMaxKHz = FreqRangesWindow.FreqRanges.FreqMaxKHz * 1000;
                           
                            // Событие изменения одной записи
                            OnChangeRecord(this, new TableEvent(FindTypeFreqRanges(FreqRangesWindow.FreqRanges)));
                        }
                    }
                }
            }
            catch { }
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableFreqRanges)DgvFreqRanges.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TableFreqRanges tableFreqRanges = new TableFreqRanges
                    {
                        Id = ((TableFreqRanges)DgvFreqRanges.SelectedItem).Id
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent(FindTypeFreqRanges(tableFreqRanges)));
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, FindNameTableFreqRanges());
            }
            catch { }
        }

        private void DgvFreqRanges_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (((TableFreqRanges)DgvFreqRanges.SelectedItem).Id > 0)
                {
                    if (((TableFreqRanges)DgvFreqRanges.SelectedItem).IsActive)
                    {
                        ((TableFreqRanges)DgvFreqRanges.SelectedItem).IsActive = false;
                    }
                    else
                    {
                        ((TableFreqRanges)DgvFreqRanges.SelectedItem).IsActive = true;
                    }

                    // Событие изменения одной записи
                    OnChangeRecord(this, new TableEvent(FindTypeFreqRanges((TableFreqRanges)DgvFreqRanges.SelectedItem)));
                }
                else
                {
                    CheckBox chbIsChecked = sender as CheckBox;
                    chbIsChecked.IsChecked = false;
                }
            }
            catch { }
        }
    }
}
