﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FreqRangesControl
{
    public class GlobalFreqRanges
    {
        public ObservableCollection<TableFreqRanges> CollectionFreqRanges { get; set; }

        public GlobalFreqRanges()
        {
            try
            {
                CollectionFreqRanges = new ObservableCollection<TableFreqRanges> { };

                //CollectionFreqRanges = new ObservableCollection<TableFreqRanges>
                //{
                //    new TableFreqRanges
                //    {
                //        Id = 1,
                //        FreqMinKHz = 56999.98,
                //        FreqMaxKHz = 78900.88,
                //        Note = "Jk8 dflk okl"
                //    },
                //    new TableFreqRanges
                //    {
                //        Id = 2,
                //        FreqMinKHz = 455999.58,
                //        FreqMaxKHz = 788900.78,
                //        Note = "jkdsgj 878"
                //    }
                //};
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
