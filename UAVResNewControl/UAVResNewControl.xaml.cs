﻿using HeightHistoryPlot;
using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TableEvents;

namespace UAVResNewControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlUAVResNew : UserControl
    {
        #region Events
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnArchiveRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEventReport> OnAddTableToReport = (object sender, TableEventReport data) => { };
        public event EventHandler<CenteringEvent> OnCentering = (object sender, CenteringEvent data) => { };
        public event EventHandler OnSaveTracks;
        #endregion

        #region Properties
        public UAVResBase NameTable { get; set; } = UAVResBase.UAVRes;
        public UAVResTrajectory NameTableTrajectory { get; set; } = UAVResTrajectory.UAVTrajectory;
        #endregion
        public UserControlUAVResNew()
        {
            InitializeComponent();

            LBUAVResNewAll.DataContext = new GlobalUAVResNewAll();
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableUAVResNewAll)LBUAVResNewAll.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TableUAVBase tableUAVRes = new TableUAVBase
                    {
                        Id = ((TableUAVResNewAll)LBUAVResNewAll.SelectedItem).TableUAVRes.Id,
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent(FindTypeUAVResBase(tableUAVRes)));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, FindNameTableUAVResBase());
            }
            catch { }
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableUAVResNewAll)LBUAVResNewAll.SelectedItem == null) return;

                if (((TableUAVResNewAll)LBUAVResNewAll.SelectedItem).TableUAVRes.Id > -1)
                {
                    if (((TableUAVResNewAll)LBUAVResNewAll.SelectedItem).TableUAVRes.IsActive)
                    {
                        ((TableUAVResNewAll)LBUAVResNewAll.SelectedItem).TableUAVRes.IsActive = true;// false;
                    }
                    else
                    {
                        ((TableUAVResNewAll)LBUAVResNewAll.SelectedItem).TableUAVRes.IsActive = false;// true;
                    }

                    // Событие изменения одной записи
                    OnChangeRecord(this, new TableEvent(FindTypeUAVResBase(((TableUAVResNewAll)LBUAVResNewAll.SelectedItem).TableUAVRes)));
                }
                else
                {
                    CheckBox chbIsChecked = sender as CheckBox;
                    chbIsChecked.IsChecked = true;// false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private void LBUAVResNewAll_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if ((TableUAVResNewAll)LBUAVResNewAll.SelectedItem == null) return;

                if (((TableUAVResNewAll)LBUAVResNewAll.SelectedItem).TableUAVRes.Id > -1)
                {
                    if (((TableUAVResNewAll)LBUAVResNewAll.SelectedItem).TableUAVRes.Id != PropNumUAV.SelectedIdUAVRes)
                    {
                        int ind = ((GlobalUAVResNewAll)LBUAVResNewAll.DataContext).ListUAVResNewAll.ToList().FindIndex(x => x.TableUAVRes.Id == ((TableUAVResNewAll)LBUAVResNewAll.SelectedItem).TableUAVRes.Id);
                        if (ind != -1)
                        {
                            PropNumUAV.SelectedNumUAVRes = ind;
                            PropNumUAV.SelectedIdUAVRes = ((TableUAVResNewAll)LBUAVResNewAll.SelectedItem).TableUAVRes.Id;
                        }
                    }
                }
                else
                {
                    PropNumUAV.SelectedNumUAVRes = 0;
                    PropNumUAV.SelectedIdUAVRes = 0;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private void LBUAVResNewAll_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if ((TableUAVResNewAll)LBUAVResNewAll.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    // Событие Центрирование карты по выбранному источнику
                    OnCentering(this, new CenteringEvent(((TableUAVResNewAll)LBUAVResNewAll.SelectedItem).TableUAVTrajectory.Coordinates));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private void ButtonArchive_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableUAVResNewAll)LBUAVResNewAll.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TableUAVBase tableUAVRes = new TableUAVBase
                    {
                        Id = ((TableUAVResNewAll)LBUAVResNewAll.SelectedItem).TableUAVRes.Id,
                        IsActive = false,
                        TrackTime = ((TableUAVResNewAll)LBUAVResNewAll.SelectedItem).TableUAVRes.TrackTime,
                        Type = ((TableUAVResNewAll)LBUAVResNewAll.SelectedItem).TableUAVRes.Type
                    };

                    // Отправить в таблицу ИРИ БПЛА архив
                    OnArchiveRecord(this, new TableEvent(tableUAVRes.ToUAVResArchive()));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private void ButtonSaveTracks_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnSaveTracks?.Invoke(sender, e);
            }
            catch { }
        }

        private void HistoryControl_Loaded(object sender, RoutedEventArgs e)
        {
            /*((GlobalUAVResNewAll)LBUAVResNewAll.DataContext).ListUAVResNewAll[0].TableUAVHeightHistory = new List<float>()
            {
                2, 4, 8, 16, 32, 128, 512, 707, 54, 23, 53, 76, 33, 32, 30, 23
            };
            ((GlobalUAVResNewAll)LBUAVResNewAll.DataContext).ListUAVResNewAll[1].TableUAVHeightHistory = new List<float>()
            {
                20, 21, 22, 22, 23, 21, 23, 23, 24, 25, 26, 27, 28, 16, 17, 14
            };
            ((GlobalUAVResNewAll)LBUAVResNewAll.DataContext).ListUAVResNewAll[2].TableUAVHeightHistory = new List<float>()
            {
                5543, 45346, 2342, 4123, 542, 35, 6347, 68546, 23
            };*/
        }
    }
}
