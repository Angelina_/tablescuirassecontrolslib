﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace UAVResNewControl
{
    public class TableUAVResNewAll : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                //Console.WriteLine($"OnPropertyChanged {prop}: {DateTime.Now.ToShortTimeString()}");
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        private TableUAVBase tableUAVRes = new TableUAVBase();
        private TableTrajectory tableUAVTrajectory = new TableTrajectory();
        private List<float> tableUAVHeightHistory = new List<float>();

        public TableUAVBase TableUAVRes
        {
            get => tableUAVRes;
            set
            {
                if (tableUAVRes == value) return;
                tableUAVRes = value;
                OnPropertyChanged();
            }
        }
        public TableTrajectory TableUAVTrajectory
        {
            get => tableUAVTrajectory;
            set
            {
                if (tableUAVTrajectory == value) return;
                tableUAVTrajectory = value;
                OnPropertyChanged();
            }
        }

        public List<float> TableUAVHeightHistory
        {
            get => this.tableUAVHeightHistory;
            set
            {
                if (this.tableUAVHeightHistory == value) return;
                this.tableUAVHeightHistory = value;
                OnPropertyChanged();
            }
        }
    }
      
    public class GlobalUAVResNewAll 
    {
        public ObservableCollection<TableUAVResNewAll> ListUAVResNewAll { get; set; }

        public GlobalUAVResNewAll()
        {
            try
            {
                ListUAVResNewAll = new ObservableCollection<TableUAVResNewAll> { };

                #region Test
                /*ListUAVResNewAll = new ObservableCollection<TableUAVResNewAll>
                {
                    new TableUAVResNewAll
                    {
                        TableUAVRes = new TableUAVRes
                        {
                            Id = 1,
                            IsActive = false,
                            State = (byte)1,
                            Type = TypeUAVRes.G3,
                            TypeM = TypeM.Drone,
                            TypeL = TypeL.Enemy,
                            Flight = (byte)1,
                            Note = "Jk8 dflk okl",
                            TrackTime = DateTime.Now
                        },
                        TableUAVTrajectory = new TableUAVTrajectory
                        {
                            FrequencyKHz = 55455.9,
                            BandKHz = 34.6F,
                            Coordinates = new Coord() { Altitude = 1, Latitude = -53.123445, Longitude = 27.456459 },
                            Elevation = 230,
                            Time = DateTime.Now
                        },
                        TableUAVHeightHistory = new List<float>()
                        {
                            54, 61, 71, 80, 24, 23, 32, 36, 41, 44, 56
                        }
                    },
                    new TableUAVResNewAll
                    {
                        TableUAVRes = new TableUAVRes
                        {
                            Id = 2,
                            IsActive = true,
                            State = (byte)0,
                            Type = TypeUAVRes.Lightbridge,
                            TypeM = TypeM.Unknown,
                            TypeL = TypeL.Friend,
                            Flight = (byte)0,
                            Note = "jkdsgj 878",
                            TrackTime = DateTime.Now
                        },
                        TableUAVTrajectory = new TableUAVTrajectory
                        {
                            FrequencyKHz = 4688795.788,
                            BandKHz = 67.6F,
                            Coordinates = new Coord() { Altitude = 211, Latitude = 56.14577777884, Longitude = -27.845869 },
                            Elevation = 50,
                            Time = DateTime.Now
                        },
                        TableUAVHeightHistory = new List<float>()
                        {
                            14, 21, 31, 35, 36, 39, 43, 44, 46, 32, 30, 29, 21, 15, 6
                        }
                    },
                    new TableUAVResNewAll
                    {
                        TableUAVRes = new TableUAVRes
                        {
                            Id = 3,
                            IsActive = true,
                            State = (byte)0,
                            Type = TypeUAVRes.Unknown,
                            TypeM = TypeM.Unknown,
                            TypeL = TypeL.Enemy,
                            Flight = (byte)0,
                            Note = "dsgsdg",
                            TrackTime = DateTime.Now
                        },
                        TableUAVTrajectory = new TableUAVTrajectory
                        {
                            FrequencyKHz = 66895.788,
                            BandKHz = 78.6F,
                            Coordinates = new Coord() { Altitude = 333, Latitude = 57.14577777884, Longitude = -23.845869 },
                            Elevation = 90,
                            Time = DateTime.Now
                        },
                        TableUAVHeightHistory = new List<float>()
                        {
                            2, 3, 9, 18, 27, 54, 108, 118, 121, 100, 94, 76, 54, 32
                        }
                    }
                };*/

                #endregion

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
       
    }
   
}
 
   

    