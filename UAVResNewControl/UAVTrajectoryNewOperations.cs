﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace UAVResNewControl
{
    public partial class UserControlUAVResNew : UserControl
    {
        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listUAVTrajectory"></param>
        public void AddUAVTrajectory(List<TableTrajectory> listUAVTrajectory)
        {
            try
            {
                if (listUAVTrajectory.Count > 0)
                {
                    for (int i = 0; i < ((GlobalUAVResNewAll)LBUAVResNewAll.DataContext).ListUAVResNewAll.Count; i++)
                    {
                        if (((GlobalUAVResNewAll)LBUAVResNewAll.DataContext).ListUAVResNewAll[i].TableUAVRes.Id != -2)
                        {
                            List<TableTrajectory> list = listUAVTrajectory.Where(x => x.TableUAVResId == ((GlobalUAVResNewAll)LBUAVResNewAll.DataContext).ListUAVResNewAll[i].TableUAVRes.Id).ToList();
                            if (list.Count != 0)
                            {
                                IEnumerable<TableTrajectory> SortNum = list.OrderBy(x => x.Num); // сортировка по номеру точки

                                List<float> elevationList = SortNum.Select(x => x.Elevation).ToList();

                                TableTrajectory trackMaxNum = SortNum.ToList().Last();

                                ((GlobalUAVResNewAll)LBUAVResNewAll.DataContext).ListUAVResNewAll[i].TableUAVTrajectory = trackMaxNum;
                                ((GlobalUAVResNewAll)LBUAVResNewAll.DataContext).ListUAVResNewAll[i].TableUAVHeightHistory = elevationList;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        /// <summary>
        /// Преобразование TableUAVTrajectory к TableUAVResAll
        /// </summary>
        /// <param name="listUAVTrajectory"></param>
        /// <returns> List<TableUAVResAll> </returns>
        private List<TableUAVResNewAll> TableTrajectoryToTableUAVResAll(List<TableTrajectory> listUAVTrajectory)
        {
            List<TableUAVResNewAll> list = new List<TableUAVResNewAll>();
            for (int i = 0; i < listUAVTrajectory.Count; i++)
            {
                TableUAVResNewAll table = new TableUAVResNewAll();
                table.TableUAVTrajectory.FrequencyKHz = listUAVTrajectory[i].FrequencyKHz;
                table.TableUAVTrajectory.BandKHz = listUAVTrajectory[i].BandKHz;
                table.TableUAVTrajectory.Coordinates.Latitude = listUAVTrajectory[i].Coordinates.Latitude;
                table.TableUAVTrajectory.Coordinates.Longitude = listUAVTrajectory[i].Coordinates.Longitude;
                table.TableUAVTrajectory.Coordinates.Altitude = listUAVTrajectory[i].Coordinates.Altitude;
                table.TableUAVTrajectory.Elevation = listUAVTrajectory[i].Elevation;
                table.TableUAVTrajectory.Time = listUAVTrajectory[i].Time;

                list.Add(table);
            }

            return list;
        }

        private TableUAVResNewAll TableUAVTrajectoryToTableUAVResAll(TableTrajectory tableUAVTrajectory)
        {
            TableUAVResNewAll table = new TableUAVResNewAll();
            table.TableUAVTrajectory.FrequencyKHz = tableUAVTrajectory.FrequencyKHz;
            table.TableUAVTrajectory.BandKHz = tableUAVTrajectory.BandKHz;
            table.TableUAVTrajectory.Coordinates.Latitude = tableUAVTrajectory.Coordinates.Latitude;
            table.TableUAVTrajectory.Coordinates.Longitude = tableUAVTrajectory.Coordinates.Longitude;
            table.TableUAVTrajectory.Coordinates.Altitude = tableUAVTrajectory.Coordinates.Altitude;
            table.TableUAVTrajectory.Elevation = tableUAVTrajectory.Elevation;
            table.TableUAVTrajectory.Time = tableUAVTrajectory.Time;

            return table;
        }
    }
}
