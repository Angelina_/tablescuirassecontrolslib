﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TableEvents;

namespace UAVResNewControl
{
    public partial class UserControlUAVResNew : UserControl
    {
        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="listUAVRes"></param>
        public void UpdateUAVRes(List<TableUAVBase> listUAVRes)
        {
            try
            {
                if (listUAVRes == null)
                    return;

                ((GlobalUAVResNewAll)LBUAVResNewAll.DataContext).ListUAVResNewAll.Clear();

                List<TableUAVResNewAll> list = TableUAVResBaseToTableUAVResNewAll(listUAVRes);

                for (int i = 0; i < listUAVRes.Count; i++)
                {
                    ((GlobalUAVResNewAll)LBUAVResNewAll.DataContext).ListUAVResNewAll.Add(list[i]);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listUAVRes"></param>
        public void AddUAVRes(List<TableUAVBase> listUAVRes)
        {
            try
            {
                List<TableUAVResNewAll> list = TableUAVResBaseToTableUAVResNewAll(listUAVRes);

                for (int i = 0; i < list.Count; i++)
                {
                    int ind = ((GlobalUAVResNewAll)LBUAVResNewAll.DataContext).ListUAVResNewAll.ToList().FindIndex(x => x.TableUAVRes.Id == list[i].TableUAVRes.Id);
                    if (ind != -1)
                    {
                        ((GlobalUAVResNewAll)LBUAVResNewAll.DataContext).ListUAVResNewAll[ind].TableUAVRes = list[i].TableUAVRes;
                    }
                    else
                    {
                        ((GlobalUAVResNewAll)LBUAVResNewAll.DataContext).ListUAVResNewAll.Add(list[i]);
                    }
                }

                LBUAVResNewAll.SelectedIndex = PropNumUAV.SelectedNumUAVRes;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        /// <summary>
        /// Обновить время наблюдения (TrackTime)
        /// </summary>
        /// <param name="listUAVRes"></param>
        public List<TableUAVBase> UpdateTrackTimeUAVRes(List<TableTrajectory> listUAVTrajectory)
        {
            List<TableUAVBase> listTrackTime = new List<TableUAVBase>();
            try
            {
                if (listUAVTrajectory.Count > 0)
                {
                    for (int i = 0; i < ((GlobalUAVResNewAll)LBUAVResNewAll.DataContext).ListUAVResNewAll.Count; i++)
                    {
                        if (((GlobalUAVResNewAll)LBUAVResNewAll.DataContext).ListUAVResNewAll[i].TableUAVRes.Id != -2)
                        {
                            List<TableTrajectory> list = listUAVTrajectory.Where(x => x.TableUAVResId == ((GlobalUAVResNewAll)LBUAVResNewAll.DataContext).ListUAVResNewAll[i].TableUAVRes.Id).ToList();
                            if (list.Count != 0)
                            {
                                IEnumerable<TableTrajectory> SortNum = list.OrderBy(x => x.Num); // сортировка по номеру точки
                                TableTrajectory TrackTimeFirst = SortNum.ToList().First();
                                TableTrajectory trackTimeLast = SortNum.ToList().Last();

                                TimeSpan timeSpan = trackTimeLast.Time - TrackTimeFirst.Time;
                                DateTime TrackTime = Convert.ToDateTime(timeSpan.ToString());

                                //string TimeFirst = TrackTimeFirst.Time.Hour.ToString("00") + ":" + TrackTimeFirst.Time.Minute.ToString("00") + ":" + TrackTimeFirst.Time.Second.ToString("00");
                                //string TimeLast = trackTimeLast.Time.Hour.ToString("00") + ":" + trackTimeLast.Time.Minute.ToString("00") + ":" + trackTimeLast.Time.Second.ToString("00");
                                //string TrackTime = TimeFirst + " - " + TimeLast;
                                //string TrackTime = timeSpan.Hours.ToString("00") + ":" + timeSpan.Minutes.ToString("00") + ":" + timeSpan.Seconds.ToString("00") + "." + timeSpan.Milliseconds.ToString("000");

                                ((GlobalUAVResNewAll)LBUAVResNewAll.DataContext).ListUAVResNewAll[i].TableUAVRes.TrackTime = TrackTime;

                                TableUAVBase table = ((GlobalUAVResNewAll)LBUAVResNewAll.DataContext).ListUAVResNewAll[i].TableUAVRes;
                                listTrackTime.Add(table);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }

            return listTrackTime;
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteUAVRes(TableUAVBase tableUAVRes)
        {
            try
            {
                int index = ((GlobalUAVResNewAll)LBUAVResNewAll.DataContext).ListUAVResNewAll.ToList().FindIndex(x => x.TableUAVRes.Id == tableUAVRes.Id);
                if (index != -1)
                {
                    ((GlobalUAVResNewAll)LBUAVResNewAll.DataContext).ListUAVResNewAll.RemoveAt(index);
                }

                LBUAVResNewAll.SelectedIndex = PropNumUAV.SelectedNumUAVRes;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((TableUAVResNewAll)LBUAVResNewAll.SelectedItem).TableUAVRes.Id == -2)
                    return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }

            return true;
        }

        /// <summary>
        /// Определить имя таблицы
        /// </summary>
        /// <returns></returns>
        private NameTable FindNameTableUAVResBase()
        {
            switch (NameTable)
            {
                case UAVResBase.UAVRes:
                    return KirasaModelsDBLib.NameTable.TableUAVRes;
                case UAVResBase.UAVResArchive:
                    return KirasaModelsDBLib.NameTable.TableUAVResArchive;
                default:
                    break;
            }
            return KirasaModelsDBLib.NameTable.TableUAVRes;
        }

        /// <summary>
        /// Определить тип таблицы
        /// </summary>
        /// <param name="sr"></param>
        /// <returns></returns>
        private AbstractCommonTable FindTypeUAVResBase(TableUAVBase table)
        {
            switch (NameTable)
            {
                case UAVResBase.UAVRes:
                    return table.ToUAVRes();
                case UAVResBase.UAVResArchive:
                    return table.ToUAVResArchive();
                default:
                    break;
            }
            return null;
        }

        /// <summary>
        /// Преобразование TableUAVRes к TableUAVResAll
        /// </summary>
        /// <param name="listUAVRes"></param>
        /// <returns> List<TableUAVResAll> </returns>
        private List<TableUAVResNewAll> TableUAVResBaseToTableUAVResNewAll(List<TableUAVBase> listUAVRes)
        {
            List<TableUAVResNewAll> list = new List<TableUAVResNewAll>();

            try
            {
                for (int i = 0; i < listUAVRes.Count; i++)
                {
                    TableUAVResNewAll table = new TableUAVResNewAll();
                    table.TableUAVRes.Id = listUAVRes[i].Id;
                    table.TableUAVRes.IsActive = listUAVRes[i].IsActive;
                    table.TableUAVRes.State = listUAVRes[i].State;
                    table.TableUAVRes.Type = listUAVRes[i].Type;
                    table.TableUAVRes.Note = listUAVRes[i].Note;
                    table.TableUAVRes.TrackTime = listUAVRes[i].TrackTime;

                    list.Add(table);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
            return list;
        }
    }
}
