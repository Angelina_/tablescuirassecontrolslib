﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UAVResControl
{
    public enum UAVResBase
    {
        UAVRes,
        UAVResArchive
    }

    public enum UAVResTrajectory
    {
        UAVTrajectory,
        UAVTrajectoryArchive
    }
}
