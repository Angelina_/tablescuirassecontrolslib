﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TableEvents;
using TableOperations;

namespace UAVResControl
{
    public partial class UserControlUAVRes : UserControl
    {
        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listUAVTrajectory"></param>
        public void AddUAVTrajectory(List<TableTrajectory> listUAVTrajectory)
        {
            try
            {
                if (listUAVTrajectory.Count > 0)
                {
                    for (int i = 0; i < ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll.Count; i++)
                    {
                        if (((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll[i].TableUAVRes.Id != -2)
                        {
                            List<TableTrajectory> list = listUAVTrajectory.Where(x => x.TableUAVResId == ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll[i].TableUAVRes.Id).ToList();
                            if (list.Count != 0)
                            {
                                IEnumerable<TableTrajectory> SortNum = list.OrderBy(x => x.Num); // сортировка по номеру точки
    
                                TableTrajectory trackMaxNum = SortNum.ToList().Last();

                                ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll[i].TableUAVTrajectory = trackMaxNum;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }
        
        /// <summary>
        /// Преобразование TableUAVTrajectory к TableUAVResAll
        /// </summary>
        /// <param name="listUAVTrajectory"></param>
        /// <returns> List<TableUAVResAll> </returns>
        private List<TableUAVResAll> TableTrajectoryToTableUAVResAll(List<TableTrajectory> listUAVTrajectory)
        {
            List<TableUAVResAll> list = new List<TableUAVResAll>();
            for (int i = 0; i < listUAVTrajectory.Count; i++)
            {
                TableUAVResAll table = new TableUAVResAll();
                table.TableUAVTrajectory.FrequencyKHz = listUAVTrajectory[i].FrequencyKHz;
                table.TableUAVTrajectory.BandKHz = listUAVTrajectory[i].BandKHz;
                table.TableUAVTrajectory.Coordinates.Latitude = listUAVTrajectory[i].Coordinates.Latitude;
                table.TableUAVTrajectory.Coordinates.Longitude = listUAVTrajectory[i].Coordinates.Longitude;
                table.TableUAVTrajectory.Coordinates.Altitude = listUAVTrajectory[i].Coordinates.Altitude;
                table.TableUAVTrajectory.Elevation = listUAVTrajectory[i].Elevation;
                table.TableUAVTrajectory.Time = listUAVTrajectory[i].Time;

                list.Add(table);
            }

            return list;
        }

        private TableUAVResAll TableUAVTrajectoryToTableUAVResAll(TableTrajectory tableUAVTrajectory)
        {
            TableUAVResAll table = new TableUAVResAll();
            table.TableUAVTrajectory.FrequencyKHz = tableUAVTrajectory.FrequencyKHz;
            table.TableUAVTrajectory.BandKHz = tableUAVTrajectory.BandKHz;
            table.TableUAVTrajectory.Coordinates.Latitude = tableUAVTrajectory.Coordinates.Latitude;
            table.TableUAVTrajectory.Coordinates.Longitude = tableUAVTrajectory.Coordinates.Longitude;
            table.TableUAVTrajectory.Coordinates.Altitude = tableUAVTrajectory.Coordinates.Altitude;
            table.TableUAVTrajectory.Elevation = tableUAVTrajectory.Elevation;
            table.TableUAVTrajectory.Time = tableUAVTrajectory.Time;

            return table;
        }
    }
}
