﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media;
using TableEvents;

namespace UAVResControl
{
    public partial class UserControlUAVRes : UserControl
    {
        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="listUAVRes"></param>
        public void UpdateUAVRes(List<TableUAVBase> listUAVRes)
        {
            try
            {
                if (listUAVRes == null)
                    return;

                ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll.Clear();

                List<TableUAVResAll> list = TableUAVResBaseToTableUAVResAll(listUAVRes);

                for (int i = 0; i < listUAVRes.Count; i++)
                {
                    ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll.Add(list[i]);
                }

                AddEmptyRows();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listUAVRes"></param>
        public void AddUAVRes(List<TableUAVBase> listUAVRes)
        {
            try
            {
                DeleteEmptyRows();

                List<TableUAVResAll> list = TableUAVResBaseToTableUAVResAll(listUAVRes);

                for (int i = 0; i < list.Count; i++)
                {
                    int ind = ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll.ToList().FindIndex(x => x.TableUAVRes.Id == list[i].TableUAVRes.Id);
                    if (ind != -1)
                    {
                        ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll[ind].TableUAVRes = list[i].TableUAVRes;
                    }
                    else
                    {
                        ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll.Add(list[i]);
                    }
                }

                AddEmptyRows();

                DgvUAVResAll.SelectedIndex = PropNumUAV.SelectedNumUAVRes;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        /// <summary>
        /// Обновить время наблюдения (TrackTime)
        /// </summary>
        /// <param name="listUAVRes"></param>
        public List<TableUAVBase> UpdateTrackTimeUAVRes(List<TableTrajectory> listUAVTrajectory)
        {
            List<TableUAVBase> listTrackTime = new List<TableUAVBase>();
            try
            {
                if (listUAVTrajectory.Count > 0)
                {
                    for (int i = 0; i < ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll.Count; i++)
                    {
                        if (((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll[i].TableUAVRes.Id != -2)
                        {
                            List<TableTrajectory> list = listUAVTrajectory.Where(x => x.TableUAVResId == ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll[i].TableUAVRes.Id).ToList();
                            if (list.Count != 0)
                            {
                                IEnumerable<TableTrajectory> SortNum = list.OrderBy(x => x.Num); // сортировка по номеру точки
                                TableTrajectory TrackTimeFirst = SortNum.ToList().First();
                                TableTrajectory trackTimeLast = SortNum.ToList().Last();

                                TimeSpan timeSpan = trackTimeLast.Time - TrackTimeFirst.Time;
                                DateTime TrackTime = Convert.ToDateTime(timeSpan.ToString());
                                
                                //string TimeFirst = TrackTimeFirst.Time.Hour.ToString("00") + ":" + TrackTimeFirst.Time.Minute.ToString("00") + ":" + TrackTimeFirst.Time.Second.ToString("00");
                                //string TimeLast = trackTimeLast.Time.Hour.ToString("00") + ":" + trackTimeLast.Time.Minute.ToString("00") + ":" + trackTimeLast.Time.Second.ToString("00");
                                //string TrackTime = TimeFirst + " - " + TimeLast;
                                //string TrackTime = timeSpan.Hours.ToString("00") + ":" + timeSpan.Minutes.ToString("00") + ":" + timeSpan.Seconds.ToString("00") + "." + timeSpan.Milliseconds.ToString("000");

                                ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll[i].TableUAVRes.TrackTime = TrackTime;

                                TableUAVBase table = ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll[i].TableUAVRes;
                                listTrackTime.Add(table);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }

            return listTrackTime;
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteUAVRes(TableUAVBase tableUAVRes)
        {
            try
            {
                int index = ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll.ToList().FindIndex(x => x.TableUAVRes.Id == tableUAVRes.Id);
                if (index != -1)
                {
                    ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll.RemoveAt(index);
                }

                AddEmptyRows();

                DgvUAVResAll.SelectedIndex = PropNumUAV.SelectedNumUAVRes;
            }
            catch(Exception ex) 
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearUAVRes()
        {
            try
            {
                ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvUAVResAll.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvUAVResAll.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvUAVResAll.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll.ToList().FindIndex(x => x.TableUAVRes.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll.RemoveAt(index);
                    }
                }

                List<TableUAVResAll> list = new List<TableUAVResAll>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    TableUAVResAll strTable = new TableUAVResAll
                    {
                        TableUAVRes = new TableUAVBase
                        {
                            Id = -2,
                            IsActive = false,
                            State = 2,
                            Type = (TypeUAVRes)255,
                            TypeM = (TypeM)255,
                            TypeL = (TypeL)255,
                            Flight = 2,
                            Note = string.Empty
                            
                        },
                        TableUAVTrajectory = new TableTrajectory
                        {
                            BandKHz = -2,
                            Coordinates = new Coord() { Altitude = -2, Latitude = -2, Longitude = -2 },
                            Elevation = -2,
                        }
                    };

                    list.Add(strTable);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll.Add(list[i]);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll.Count(s => s.TableUAVRes.Id < 0);
                int countAllRows = ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll.RemoveAt(iCount);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((TableUAVResAll)DgvUAVResAll.SelectedItem).TableUAVRes.Id == -2)
                    return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }

            return true;
        }

        SolidColorBrush solidBrushBlue = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFC6FAFF")); // бледно-голубой (активная строка)
        SolidColorBrush solidBrushGray = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF7C7C7C")); // бледно-серый (неактивная строка)

        /// <summary>
        /// Обозначить состояние серым цветом (неактивные ИРИ)
        /// </summary>
        /// <param name="listUAVRes"></param>
        public void ColorRowState(List<TableUAVBase> listUAVRes)
        {
            try
            {
                for (int row = 0; row < DgvUAVResAll.Items.Count; row++)
                {
                    var dataGridRow = DgvUAVResAll.ItemContainerGenerator.ContainerFromItem(DgvUAVResAll.Items[row]) as DataGridRow;

                    if (dataGridRow != null)
                    {
                        dataGridRow.Foreground = solidBrushBlue;
                    }
                }

                for (int row = 0; row < DgvUAVResAll.Items.Count; row++)
                {
                    var dataGridRow = DgvUAVResAll.ItemContainerGenerator.ContainerFromItem(DgvUAVResAll.Items[row]) as DataGridRow;

                    if (dataGridRow != null)
                    {
                        var State = (dataGridRow.Item as TableUAVResAll).TableUAVRes.State;
                        for (int i = 0; i < listUAVRes.Count; i++)
                        {
                            if (State == 0)
                            {
                                dataGridRow.Foreground = solidBrushGray;
                            }
                        }
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// Тип дрона из TypeUAVRes в string
        /// </summary>
        /// <param name="typeUAVRes"></param>
        /// <returns></returns>
        private string ConvertTypeDroneUAVRes(TypeUAVRes typeUAVRes)
        {
            string sType = string.Empty;

            switch (typeUAVRes)
            {
                case TypeUAVRes.Ocusinc:
                    sType = "Mavic 2";
                    break;

                case TypeUAVRes.G3:
                    sType = "3G";
                    break;

                case TypeUAVRes.Lightbridge:
                    sType = "Phantom 4";
                    break;

                case TypeUAVRes.WiFi:
                    sType = "Mavic Air";
                    break;

                case TypeUAVRes.Unknown:
                    sType = "DJI Phantom 3";
                    break;

                default:
                    sType = string.Empty;
                    break;
            }

            return sType;
        }

        /// <summary>
        /// Преобразование TableUAVRes к TableUAVResAll
        /// </summary>
        /// <param name="listUAVRes"></param>
        /// <returns> List<TableUAVResAll> </returns>
        private List<TableUAVResAll> TableUAVResBaseToTableUAVResAll(List<TableUAVBase> listUAVRes)
        {
            List<TableUAVResAll> list = new List<TableUAVResAll>();

            try
            {
                for (int i = 0; i < listUAVRes.Count; i++)
                {
                    TableUAVResAll table = new TableUAVResAll();
                    table.TableUAVRes.Id = listUAVRes[i].Id;
                    table.TableUAVRes.IsActive = listUAVRes[i].IsActive;
                    table.TableUAVRes.State = listUAVRes[i].State;
                    table.TableUAVRes.Type = listUAVRes[i].Type;
                    table.TableUAVRes.Note = listUAVRes[i].Note;
                    table.TableUAVRes.TrackTime = listUAVRes[i].TrackTime;

                    list.Add(table);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
            return list;
        }

        /// <summary>
        /// Определить имя таблицы
        /// </summary>
        /// <returns></returns>
        private NameTable FindNameTableUAVResBase()
        {
            switch (NameTable)
            {
                case UAVResBase.UAVRes:
                    return KirasaModelsDBLib.NameTable.TableUAVRes;
                case UAVResBase.UAVResArchive:
                    return KirasaModelsDBLib.NameTable.TableUAVResArchive;
                default:
                    break;
            }
            return KirasaModelsDBLib.NameTable.TableUAVRes;
        }

        /// <summary>
        /// Определить тип таблицы
        /// </summary>
        /// <param name="sr"></param>
        /// <returns></returns>
        private AbstractCommonTable FindTypeUAVResBase(TableUAVBase table)
        {
            switch (NameTable)
            {
                case UAVResBase.UAVRes:
                    return table.ToUAVRes();
                case UAVResBase.UAVResArchive:
                    return table.ToUAVResArchive();
                default:
                    break;
            }
            return null;
        }


    }
}
