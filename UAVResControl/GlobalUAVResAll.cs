﻿using KirasaModelsDBLib;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace UAVResControl
{
    public class TableUAVResAll : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                //Console.WriteLine($"OnPropertyChanged {prop}: {DateTime.Now.ToShortTimeString()}");
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        private TableUAVBase tableUAVRes = new TableUAVBase();
        private TableTrajectory tableUAVTrajectory = new TableTrajectory();
      
        public TableUAVBase TableUAVRes 
        {
            get => tableUAVRes;
            set
            {
                if (tableUAVRes == value) return;
                tableUAVRes = value;
                OnPropertyChanged();
            }
        }
        public TableTrajectory TableUAVTrajectory 
        { 
            get => tableUAVTrajectory;
            set
            {
                if (tableUAVTrajectory == value) return;
                tableUAVTrajectory = value;
                OnPropertyChanged();
            }
        }

        //void Update(TableUAVResAll updatedModel)
        //{
        //    TableUAVRes.Update(updatedModel.TableUAVRes);
        //    TableUAVTrajectory.Update(updatedModel.TableUAVTrajectory);
        //}
    }

    public class GlobalUAVResAll //: INotifyPropertyChanged
    {
      
        //#region INotifyPropertyChanged
        //public event PropertyChangedEventHandler PropertyChanged;
        //public void OnPropertyChanged([CallerMemberName]string prop = "")
        //{
        //    try
        //    {
        //        Console.WriteLine($"OnPropertyChanged {prop}: {DateTime.Now.ToShortTimeString()}");
        //        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        //    }
        //    catch { }
        //}
        //#endregion

        public ObservableCollection<TableUAVResAll> CollectionUAVResAll { get; set; }

        public GlobalUAVResAll()
        {
            try
            {
                CollectionUAVResAll = new ObservableCollection<TableUAVResAll> { };

                #region Test
                //CollectionUAVResAll = new ObservableCollection<TableUAVResAll>
                //{
                //    new TableUAVResAll
                //    {
                //        TableUAVRes = new TableUAVRes
                //        {
                //            Id = 1,
                //            IsActive = false,
                //            State = (byte)1,
                //            Type = TypeUAVRes.Ocusinc,
                //            TypeM = TypeM.Drone,
                //            TypeL = TypeL.Enemy,
                //            Flight = (byte)1,
                //            Note = "Jk8 dflk okl",
                //            TrackTime = DateTime.Now
                //        },
                //        TableUAVTrajectory = new TableUAVTrajectory
                //        {
                //            FrequencyKHz = 55455.9,
                //            BandKHz = 34.6F,
                //            Coordinates = new Coord() { Altitude = 1, Latitude = -53.123445, Longitude = 27.456459 },
                //            Elevation = 30,
                //            Time = DateTime.Now
                //        }
                //    },
                //    new TableUAVResAll
                //    {
                //        TableUAVRes = new TableUAVRes
                //        {
                //            Id = 2,
                //            IsActive = true,
                //            State = (byte)0,
                //            Type = TypeUAVRes.Lightbridge,
                //            TypeM = TypeM.Unknown,
                //            TypeL = TypeL.Friend,
                //            Flight = (byte)0,
                //            Note = "jkdsgj 878",
                //            TrackTime = DateTime.Now
                //        },
                //        TableUAVTrajectory = new TableUAVTrajectory
                //        {
                //            FrequencyKHz = 88895.788,
                //            BandKHz = 67.6F,
                //            Coordinates = new Coord() { Altitude = 211, Latitude = 56.14577777884, Longitude = -27.845869 },
                //            Elevation = 50,
                //            Time = DateTime.Now
                //        }
                //    }
                //};

                #endregion

                //CollectionUAVResAll.CollectionChanged += CollectionUAVResAll_CollectionChanged;
            }
            catch (Exception ex) 
            {
                MessageBox.Show(ex.Message);
            }
        }

        //private void CollectionUAVResAll_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        //{
        //    OnPropertyChanged(nameof(CollectionUAVResAll));
        //    Console.WriteLine($"Collection changed: {DateTime.Now.ToShortTimeString()}");
        //}
    }
}
