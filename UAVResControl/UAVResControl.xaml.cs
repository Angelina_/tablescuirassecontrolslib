﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using TableEvents;

namespace UAVResControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlUAVRes : UserControl
    {
        #region Events
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnArchiveRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEventReport> OnAddTableToReport = (object sender, TableEventReport data) => { };
        public event EventHandler<CenteringEvent> OnCentering = (object sender, CenteringEvent data) => { };
        public event EventHandler OnSaveTracks;
        #endregion

        #region Properties
        public UAVResBase NameTable { get; set; } = UAVResBase.UAVRes;
        public UAVResTrajectory NameTableTrajectory { get; set; } = UAVResTrajectory.UAVTrajectory;
        #endregion

        #region struct STBChecked
        public struct STBChecked
        {
            public int indCollection;
            public bool IsTBChecked;
            public int Id;
            public string Type;
            public string Time;
        }

        private List<STBChecked> listTBChecked = new List<STBChecked>();
        #endregion

        public UserControlUAVRes()
        {
            InitializeComponent();

            DgvUAVResAll.DataContext = new GlobalUAVResAll();
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableUAVResAll)DgvUAVResAll.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TableUAVBase tableUAVRes = new TableUAVBase
                    {
                        Id = ((TableUAVResAll)DgvUAVResAll.SelectedItem).TableUAVRes.Id,
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent(FindTypeUAVResBase(tableUAVRes)));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, FindNameTableUAVResBase());
            }
            catch { }
        }

        private void DgvUAVResAll_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void DgvUAVRes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if ((TableUAVResAll)DgvUAVResAll.SelectedItem == null) return;

                if (((TableUAVResAll)DgvUAVResAll.SelectedItem).TableUAVRes.Id > -1)
                {
                    if (((TableUAVResAll)DgvUAVResAll.SelectedItem).TableUAVRes.Id != PropNumUAV.SelectedIdUAVRes)
                    {
                        int ind = ((GlobalUAVResAll)DgvUAVResAll.DataContext).CollectionUAVResAll.ToList().FindIndex(x => x.TableUAVRes.Id == ((TableUAVResAll)DgvUAVResAll.SelectedItem).TableUAVRes.Id);
                        if (ind != -1)
                        {
                            PropNumUAV.SelectedNumUAVRes = ind;
                            PropNumUAV.SelectedIdUAVRes = ((TableUAVResAll)DgvUAVResAll.SelectedItem).TableUAVRes.Id;
                            //PropNumUAV.IsSelectedNumUAVRes = true;
                        }
                    }
                }
                else
                {
                    PropNumUAV.SelectedNumUAVRes = 0;
                    PropNumUAV.SelectedIdUAVRes = 0;
                    //PropNumUAV.IsSelectedNumUAVRes = false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //if ((TableUAVResAll)DgvUAVResAll.SelectedItem != null)
                //{
                    if (((TableUAVResAll)DgvUAVResAll.SelectedItem).TableUAVRes.Id > -1)
                    {
                        if (((TableUAVResAll)DgvUAVResAll.SelectedItem).TableUAVRes.IsActive)
                        {
                            ((TableUAVResAll)DgvUAVResAll.SelectedItem).TableUAVRes.IsActive = false;
                        }
                        else
                        {
                            ((TableUAVResAll)DgvUAVResAll.SelectedItem).TableUAVRes.IsActive = true;
                        }

                        // Событие изменения одной записи
                        OnChangeRecord(this, new TableEvent(FindTypeUAVResBase(((TableUAVResAll)DgvUAVResAll.SelectedItem).TableUAVRes)));
                    }
                //}
                else
                {
                    CheckBox chbIsChecked = sender as CheckBox;
                    chbIsChecked.IsChecked = false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private void ButtonSaveTracks_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnSaveTracks?.Invoke(sender, e);
            }
            catch { }
        }

        private void ButtonArchive_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableUAVResAll)DgvUAVResAll.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TableUAVBase tableUAVRes = new TableUAVBase
                    {
                        Id = ((TableUAVResAll)DgvUAVResAll.SelectedItem).TableUAVRes.Id,
                        IsActive = false,
                        TrackTime = ((TableUAVResAll)DgvUAVResAll.SelectedItem).TableUAVRes.TrackTime,
                        Type = ((TableUAVResAll)DgvUAVResAll.SelectedItem).TableUAVRes.Type
                    };

                    // Отправить в таблицу ИРИ БПЛА архив
                    OnArchiveRecord(this, new TableEvent(tableUAVRes.ToUAVResArchive()));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private void DgvUAVResAll_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                if ((TableUAVResAll)DgvUAVResAll.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    // Событие Центрирование карты по выбранному источнику
                    OnCentering(this, new CenteringEvent(((TableUAVResAll)DgvUAVResAll.SelectedItem).TableUAVTrajectory.Coordinates));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }
    }
}
