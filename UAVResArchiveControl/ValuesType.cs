﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UAVResArchiveControl
{
    public class ValuesType : ObservableCollection<TypeUAVRes>
    {
        public ValuesType()
        {
            Add(TypeUAVRes.Lightbridge);
            Add(TypeUAVRes.Ocusinc);
            Add(TypeUAVRes.G3);
            Add(TypeUAVRes.WiFi);
            Add(TypeUAVRes.Unknown);
        }

    }
}
