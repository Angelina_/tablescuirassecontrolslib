﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ValuesCorrectLib;

namespace UAVResArchiveControl
{
    /// <summary>
    /// Логика взаимодействия для UAVResArchiveProperty.xaml
    /// </summary>
    public partial class UAVResArchiveProperty : Window
    {
        private ObservableCollection<TableUAVBase> collectionTemp;
        public TableUAVBase UAVRes { get; private set; }
        public UAVResArchiveProperty(ObservableCollection<TableUAVBase> collectionUAVResAll, TableUAVBase tableUAVRes)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionUAVResAll;
                UAVRes = tableUAVRes;

                propertyGrid.SelectedObject = UAVRes;

                propertyGrid.Properties["TypeM"].IsBrowsable = false;
                propertyGrid.Properties["TypeL"].IsBrowsable = false;
                propertyGrid.Properties["Flight"].IsBrowsable = false;

                Title = SMeaning.meaningChangeRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/ChangeRec.ico", UriKind.Absolute));

                //ChangeCategories();
                InitProperty();
            }
            catch { }
        }

        public UAVResArchiveProperty()
        {
            InitializeComponent();

            InitEditors();
            //ChangeCategories();
            InitProperty();
        }

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void ButtonNoApply_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void InitEditors()
        {
            propertyGrid.Editors.Add(new UAVResArchiveTypeEditor(nameof(UAVRes.Type), typeof(TableUAVBase)));
            propertyGrid.Editors.Add(new UAVResArchiveNoteEditor(nameof(UAVRes.Note), typeof(TableUAVBase)));
            propertyGrid.Editors.Add(new UAVResArchiveTrackTimeEditor(nameof(UAVRes.TrackTime), typeof(TableUAVBase)));
        }

        private void InitProperty()
        {
            foreach (var property in propertyGrid.Properties)
            {
                try
                {
                    if (property.IsBrowsable == false) { continue; }

                    if (property.PropertyValue.SubProperties.Count != 0)
                    {
                        foreach (var subProperty in property.PropertyValue.SubProperties)
                            subProperty.PropertyValue.PropertyValueException += PropertyGridSubException;
                        continue;
                    }
                    property.PropertyValue.PropertyValueException += PropertyGridException;
                }
                catch (Exception ex)
                { System.Windows.MessageBox.Show(ex.Message); }
            }
        }
        private void PropertyGridSubException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void PropertyGridException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void gridProperty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                DialogResult = true;
            }
        }
        public void SetLanguagePropertyGrid(DllCuirassemProperties.Models.Languages language)
        {
            LoadTranslatorPropertyGrid(language);
            TranslatorTables.ChangeLanguagePropertyGrid(language, propertyGrid);
        }

        private void LoadTranslatorPropertyGrid(DllCuirassemProperties.Models.Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case DllCuirassemProperties.Models.Languages.EN:
                        dict.Source = new Uri("/Cuirasse;component/Languages/TranslatorTables/TranslatorTablesCuirasse.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case DllCuirassemProperties.Models.Languages.RU:
                        dict.Source = new Uri("/Cuirasse;component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/Cuirasse;component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                // TEST ------------------------------------------------------------------------------------------------------
                //switch (language)
                //{
                //    case DllCuirassemProperties.Models.Languages.EN:
                //        dict.Source = new Uri("/ProjectTablesControlTEST;component/Languages/TranslatorTables/TranslatorTablesCuirasse.EN.xaml",
                //                      UriKind.Relative);
                //        break;
                //    case DllCuirassemProperties.Models.Languages.RU:
                //        dict.Source = new Uri("/ProjectTablesControlTEST;component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                //                            UriKind.Relative);
                //        break;
                //    default:
                //        dict.Source = new Uri("/ProjectTablesControlTEST;component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                //                          UriKind.Relative);
                //        break;
                //}
                // ------------------------------------------------------------------------------------------------------ TEST

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }
    }
}
