﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TableEvents;

namespace UAVResArchiveControl
{
    class ItemEqualityComparer : IEqualityComparer<TableTrajectory>
    {
        public bool Equals(TableTrajectory x, TableTrajectory y)
        {
            return x.Time == y.Time;
        }

        public int GetHashCode(TableTrajectory obj)
        {
            return obj.Time.GetHashCode();
        }
    }

    public partial class UserControlUAVResArchive : UserControl
    {
        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="listUAVTrajectory"></param>
        public void UpdateUAVTrajectoryArchive(List<TableTrajectory> listUAVTrajectory)
        {
            try
            {
                if (listUAVTrajectory == null)
                    return;

                IEnumerable<TableTrajectory> listSortNum = listUAVTrajectory.OrderBy(x => x.Num); // сортировка по Num
                List<TableTrajectory> list = listSortNum.ToList();

                ((GlobalUAVResTrajectoryArchive)DgvUAVTrajectoryArchive.DataContext).CollectionUAVTrajectoryArchive.Clear();

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalUAVResTrajectoryArchive)DgvUAVTrajectoryArchive.DataContext).CollectionUAVTrajectoryArchive.Add(list[i]);
                }

                AddEmptyRowsTrajectory();

                DgvUAVResArchive.SelectedIndex = PropNumUAV.SelectedNumUAVResArchive;
            }
            catch { }
        }

        /// <summary>
        /// Объединить траектории
        /// </summary>
        /// <param name="listUAVTrajectory"></param>
        public List<TableTrajectory> CombineTracksArchive(List<TableTrajectory> listUAVTrajectory)
        {
            //try
            //{
               return listUAVTrajectory.OrderBy(x => x.Time).ToList().Distinct(new ItemEqualityComparer()).ToList();

                //List<TableTrajectory> listSortTime = listUAVTrajectory.OrderBy(x => x.Time).ToList();

                //List<TableTrajectory> result = listSortTime.Distinct(new ItemEqualityComparer()).ToList();
                  
            //}
            //catch { }

            
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listUAVTrajectory"></param>
        public void AddUAVTrajectoryArchive(List<TableTrajectory> listUAVTrajectory)
        {
            try
            {
                DeleteEmptyRowsTrajectory();

                for (int i = 0; i < listUAVTrajectory.Count; i++)
                {
                    int ind = ((GlobalUAVResTrajectoryArchive)DgvUAVTrajectoryArchive.DataContext).CollectionUAVTrajectoryArchive.ToList().FindIndex(x => x.Id == listUAVTrajectory[i].Id);
                    if (ind != -1)
                    {
                        ((GlobalUAVResTrajectoryArchive)DgvUAVTrajectoryArchive.DataContext).CollectionUAVTrajectoryArchive[ind] = listUAVTrajectory[i];
                    }
                    else
                    {
                        ((GlobalUAVResTrajectoryArchive)DgvUAVTrajectoryArchive.DataContext).CollectionUAVTrajectoryArchive.Add(listUAVTrajectory[i]);

                        // Save trajectory
                        //int index = listTBChecked.FindIndex(x => x.Id == listUAVTrajectory[i].TableUAVResId);
                        //if (index != -1)
                        //{
                        //    if (listTBChecked[index].IsTBChecked)
                        //    {
                        //        SaveTrajectory.SaveTrajectoryUAVRes(listTBChecked[index].Type, listTBChecked[index].Id.ToString(), listTBChecked[index].Time, listUAVTrajectory[i]);
                        //    }
                        //}
                    }
                }
                ///////
                var listSort = ((GlobalUAVResTrajectoryArchive)DgvUAVTrajectoryArchive.DataContext).CollectionUAVTrajectoryArchive.ToList();
                IEnumerable<TableTrajectory> SortNum = listSort.OrderBy(x => x.Num); // сортировка по частоте
                for (int i = 0; i < ((GlobalUAVResTrajectoryArchive)DgvUAVTrajectoryArchive.DataContext).CollectionUAVTrajectoryArchive.Count; i++)
                {
                    ((GlobalUAVResTrajectoryArchive)DgvUAVTrajectoryArchive.DataContext).CollectionUAVTrajectoryArchive[i] = SortNum.ToList()[i];
                }
                ////////
                AddEmptyRowsTrajectory();

                DgvUAVTrajectoryArchive.SelectedIndex = PropNumUAV.SelectedNumUAVResArchive;
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteUAVTrajectoryArchive(TableTrajectory tableUAVTrajectory)
        {
            try
            {
                int index = ((GlobalUAVResTrajectoryArchive)DgvUAVTrajectoryArchive.DataContext).CollectionUAVTrajectoryArchive.ToList().FindIndex(x => x.Id == tableUAVTrajectory.Id);
                ((GlobalUAVResTrajectoryArchive)DgvUAVTrajectoryArchive.DataContext).CollectionUAVTrajectoryArchive.RemoveAt(index);

                AddEmptyRowsTrajectory();
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearUAVTrajectoryArchive()
        {
            try
            {
                ((GlobalUAVResTrajectoryArchive)DgvUAVTrajectoryArchive.DataContext).CollectionUAVTrajectoryArchive.Clear();

                AddEmptyRowsTrajectory();
            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRowsTrajectory()
        {
            try
            {
                int сountRowsAll = DgvUAVTrajectoryArchive.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvUAVTrajectoryArchive.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvUAVTrajectoryArchive.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalUAVResTrajectoryArchive)DgvUAVTrajectoryArchive.DataContext).CollectionUAVTrajectoryArchive.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalUAVResTrajectoryArchive)DgvUAVTrajectoryArchive.DataContext).CollectionUAVTrajectoryArchive.RemoveAt(index);
                    }
                }

                List<TableTrajectory> list = new List<TableTrajectory>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    TableTrajectory strT = new TableTrajectory
                    {
                        Id = -2,
                        Num = -2,
                        BandKHz = -2,
                        Coordinates = new Coord() { Altitude = -2, Latitude = -2, Longitude = -2 },
                        Elevation = -2,
                    };
                    list.Add(strT);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalUAVResTrajectoryArchive)DgvUAVTrajectoryArchive.DataContext).CollectionUAVTrajectoryArchive.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRowsTrajectory()
        {
            try
            {
                int countEmptyRows = ((GlobalUAVResTrajectoryArchive)DgvUAVTrajectoryArchive.DataContext).CollectionUAVTrajectoryArchive.Count(s => s.Id < 0);
                int countAllRows = ((GlobalUAVResTrajectoryArchive)DgvUAVTrajectoryArchive.DataContext).CollectionUAVTrajectoryArchive.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalUAVResTrajectoryArchive)DgvUAVTrajectoryArchive.DataContext).CollectionUAVTrajectoryArchive.RemoveAt(iCount);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmptyTrajectory()
        {
            try
            {
                if (((TableTrajectory)DgvUAVTrajectoryArchive.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }
    }
}
