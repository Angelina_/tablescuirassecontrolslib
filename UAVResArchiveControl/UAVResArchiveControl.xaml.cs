﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TableEvents;

namespace UAVResArchiveControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlUAVResArchive : UserControl
    {
        public UAVResArchiveProperty UAVResArchiveWindow;
        public CombineTrack CombineTrackWindow;

        #region Events
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };
        public event EventHandler<ListEvents> OnDisplayTrack = (object sender, ListEvents data) => { };

        public event EventHandler<SelectedRowEvents> OnSelectedRow = (object sender, SelectedRowEvents data) => { };

        public event EventHandler<TableEventReport> OnAddTableToReport = (object sender, TableEventReport data) => { };

        // Открылось окно с PropertyGrid
        public event EventHandler<UAVResArchiveProperty> OnIsWindowPropertyOpen = (object sender, UAVResArchiveProperty data) => { };

        public event EventHandler OnSaveTracks;

        public event EventHandler<ListEvents> OnCombineTracks = (object sender, ListEvents data) => { };
        #endregion

        #region Properties
        public UAVResArchive NameTable { get; set; } = UAVResArchive.UAVResArchive;
        public UAVResTrajectoryArchive NameTableTrajectory { get; set; } = UAVResTrajectoryArchive.UAVTrajectoryArchive;
        #endregion
      
        public UserControlUAVResArchive()
        {
            InitializeComponent();

            DgvUAVResArchive.DataContext = new GlobalUAVResArchive();
            DgvUAVTrajectoryArchive.DataContext = new GlobalUAVResTrajectoryArchive();
        }

        private void ButtonChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableUAVBase)DgvUAVResArchive.SelectedItem != null)
                {
                    if (((TableUAVBase)DgvUAVResArchive.SelectedItem).Id > -1)
                    {
                        var selected = (TableUAVBase)DgvUAVResArchive.SelectedItem;

                        UAVResArchiveWindow = new UAVResArchiveProperty(((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive, selected.Clone());

                        OnIsWindowPropertyOpen(this, UAVResArchiveWindow);

                        if (UAVResArchiveWindow.ShowDialog() == true)
                        {
                            // Событие изменения одной записи
                            OnChangeRecord(this, new TableEvent(FindTypeUAVResArchive(UAVResArchiveWindow.UAVRes)));
                        }
                    }
                }
            }
            catch { }
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableUAVBase)DgvUAVResArchive.SelectedItem != null)
                {
                    if (!IsSelectedRowEmptyRes())
                        return;

                    TableUAVBase tableUAVRes = new TableUAVBase
                    {
                        Id = ((TableUAVBase)DgvUAVResArchive.SelectedItem).Id,
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent(FindTypeUAVResArchive(tableUAVRes)));

                    PropNumUAV.SelectedNumUAVResArchive = 0;
                    //int ind = ((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive.ToList().FindIndex(x => x.Id == ((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive.ToList().FirstOrDefault().Id);
                    //if (ind != -1)
                    //{
                    //    OnSelectedRow(this, new SelectedRowEvents(ind));
                    //}


                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, FindNameTableUAVResArchive());
            }
            catch { }
        }
      
        private void DgvUAVResArchive_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRowsRes();
        }
   
        private void DgvUAVTrajectoryArchive_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRowsTrajectory();
        }
       
        private void DgvUAVResArchive_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((TableUAVBase)DgvUAVResArchive.SelectedItem == null) return;

            if (((TableUAVBase)DgvUAVResArchive.SelectedItem).Id > -1)
            {
                if (((TableUAVBase)DgvUAVResArchive.SelectedItem).Id != PropNumUAV.SelectedIdUAVResArchive)
                {
                    int ind = ((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive.ToList().FindIndex(x => x.Id == ((TableUAVBase)DgvUAVResArchive.SelectedItem).Id);
                    if (ind != -1)
                    {
                        PropNumUAV.SelectedNumUAVResArchive = ind;
                        PropNumUAV.SelectedIdUAVResArchive = ((TableUAVBase)DgvUAVResArchive.SelectedItem).Id;
                        //PropNumUAV.IsSelectedNumUAVResArchive = true;

                        OnSelectedRow(this, new SelectedRowEvents(PropNumUAV.SelectedIdUAVResArchive));
                    }
                }
            }
            else
            {
                PropNumUAV.SelectedNumUAVResArchive = 0;
                PropNumUAV.SelectedIdUAVResArchive = 0;
                //PropNumUAV.IsSelectedNumUAVResArchive = false;
            }
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (((TableUAVBase)DgvUAVResArchive.SelectedItem).Id > -1)
                {
                    if (((TableUAVBase)DgvUAVResArchive.SelectedItem).IsActive)
                    {
                        ((TableUAVBase)DgvUAVResArchive.SelectedItem).IsActive = false;
                    }
                    else
                    {
                        ((TableUAVBase)DgvUAVResArchive.SelectedItem).IsActive = true;
                    }

                    // Событие изменения одной записи
                    OnChangeRecord(this, new TableEvent(FindTypeUAVResArchive((TableUAVBase)DgvUAVResArchive.SelectedItem)));
                }
                else
                {
                    CheckBox chbIsChecked = sender as CheckBox;
                    chbIsChecked.IsChecked = false;
                }
            }
            catch { }
        }

        private void ButtonSaveTracks_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnSaveTracks?.Invoke(sender, e);
            }
            catch { }
        }

        private void ButtonСombineTrack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CombineTrackWindow = new CombineTrack();

                if (CombineTrackWindow.ShowDialog() == true)
                {
                    //string str = CombineTrackWindow.cbType.SelectedValue.ToString();

                    List<TableUAVBase> lSelectedType = ((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive.ToList().Where(x => x.IsActive == true).ToList();

                    if (lSelectedType.Count > 0)
                    {
                        // Событие объединения траекторий
                        OnCombineTracks(this, new ListEvents(lSelectedType, (TypeUAVRes)CombineTrackWindow.cbType.SelectedValue, KirasaModelsDBLib.NameTable.TableUAVResArchive));
                    }
                }
            }
            catch { }
        }

        private void ButtonDisplayTrack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<TableUAVBase> list = new List<TableUAVBase>();

                for(int i = 0; i < ((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive.Count; i++)
                {
                    if(((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive[i].Id != -2)
                    {
                        if (((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive[i].IsActive == true)
                        {
                            TableUAVBase table = ((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive[i];
                            list.Add(table);
                        }
                    }
                }
                if(list.Count > 0)
                {
                    OnDisplayTrack(this, new ListEvents(list, TypeUAVRes.Unknown, KirasaModelsDBLib.NameTable.TableUAVResArchive));
                }
                
            }
            catch { }
        }

        private void DgvUAVTrajectoryArchive_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            try
            {
                // Отменяет выделение всех ячеек в элементе управления
                DgvUAVTrajectoryArchive.UnselectAllCells();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }
    }
}
