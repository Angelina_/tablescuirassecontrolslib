﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ValuesCorrectLib;

namespace UAVResArchiveControl
{
    /// <summary>
    /// Логика взаимодействия для CombineTrack.xaml
    /// </summary>
    public partial class CombineTrack : Window
    {
        public CombineTrack()
        {
            InitializeComponent();

            TranslationWnd();
        }

        private void TranslationWnd()
        {
            lTypeSource.Content = SWndCombineTrack.labelTypeSource;
            lUnionTracks.Content = SWndCombineTrack.labelUnionTracks;
        }

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void ButtonNoApply_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void gridCombineTrack_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                DialogResult = true;
            }
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

       
    }
}
