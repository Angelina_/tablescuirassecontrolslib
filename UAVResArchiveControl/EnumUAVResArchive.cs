﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UAVResArchiveControl
{
    public enum UAVResArchive
    {
        UAVRes,
        UAVResArchive
    }

    public enum UAVResTrajectoryArchive
    {
        UAVTrajectory,
        UAVTrajectoryArchive
    }
}
