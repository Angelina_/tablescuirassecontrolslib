﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using TableEvents;

namespace UAVResArchiveControl
{
    public partial class UserControlUAVResArchive : UserControl
    {
        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="listUAVRes"></param>
        public void UpdateUAVResArchive(List<TableUAVBase> listUAVRes)
        {
            try
            {
                if (listUAVRes == null)
                    return;

                ((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive.Clear();


                for (int i = 0; i < listUAVRes.Count; i++)
                {
                    ((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive.Add(listUAVRes[i]);
                }

                AddEmptyRowsRes();

                DgvUAVResArchive.SelectedIndex = PropNumUAV.SelectedNumUAVResArchive;
            }
            catch { }
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listUAVRes"></param>
        public void AddUAVResArchive(List<TableUAVBase> listUAVRes)
        {
            try
            {
                DeleteEmptyRowsRes();

                for (int i = 0; i < listUAVRes.Count; i++)
                {
                    int ind = ((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive.ToList().FindIndex(x => x.Id == listUAVRes[i].Id);
                    if (ind != -1)
                    {
                        ((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive[ind] = listUAVRes[i];
                    }
                    else
                    {
                        ((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive.Add(listUAVRes[i]);
                    }
                }

                AddEmptyRowsRes();

                DgvUAVResArchive.SelectedIndex = PropNumUAV.SelectedNumUAVResArchive;
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteUAVResArchive(TableUAVBase tableUAVRes)
        {
            try
            {
                int index = ((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive.ToList().FindIndex(x => x.Id == tableUAVRes.Id);
                if (index != -1)
                {
                    ((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive.RemoveAt(index);
                }

                AddEmptyRowsRes();

                DgvUAVResArchive.SelectedIndex = PropNumUAV.SelectedNumUAVResArchive;
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearUAVResArchive()
        {
            try
            {
                ((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive.Clear();

                AddEmptyRowsRes();
            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRowsRes()
        {
            try
            {
                int сountRowsAll = DgvUAVResArchive.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvUAVResArchive.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvUAVResArchive.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive.RemoveAt(index);
                    }
                }

                List<TableUAVBase> list = new List<TableUAVBase>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    TableUAVBase strSR = new TableUAVBase
                    {
                        Id = -2,
                        IsActive = false,
                        State = 2,
                        Type = (TypeUAVRes)255,
                        Note = string.Empty
                    };
                    list.Add(strSR);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRowsRes()
        {
            try
            {
                int countEmptyRows = ((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive.Count(s => s.Id < 0);
                int countAllRows = ((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalUAVResArchive)DgvUAVResArchive.DataContext).CollectionUAVResArchive.RemoveAt(iCount);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmptyRes()
        {
            try
            {
                if (((TableUAVBase)DgvUAVResArchive.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }

        SolidColorBrush solidBrushBlue = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFC6FAFF")); // бледно-голубой (активная строка)
        SolidColorBrush solidBrushGray = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF7C7C7C")); // бледно-серый (неактивная строка)

        /// <summary>
        /// Обозначить состояние серым цветом (неактивные ИРИ)
        /// </summary>
        /// <param name="listUAVRes"></param>
        public void ColorRowState(List<TableUAVBase> listUAVRes)
        {
            try
            {
                for (int row = 0; row < DgvUAVResArchive.Items.Count; row++)
                {
                    var dataGridRow = DgvUAVResArchive.ItemContainerGenerator.ContainerFromItem(DgvUAVResArchive.Items[row]) as DataGridRow;

                    if (dataGridRow != null)
                    {
                        dataGridRow.Foreground = solidBrushBlue;
                    }
                }

                for (int row = 0; row < DgvUAVResArchive.Items.Count; row++)
                {
                    var dataGridRow = DgvUAVResArchive.ItemContainerGenerator.ContainerFromItem(DgvUAVResArchive.Items[row]) as DataGridRow;

                    if (dataGridRow != null)
                    {
                        var State = (dataGridRow.Item as TableUAVBase).State;
                        for (int i = 0; i < listUAVRes.Count; i++)
                        {
                            if (State == 0)
                            {
                                dataGridRow.Foreground = solidBrushGray;
                            }
                        }
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// Тип дрона из TypeUAVRes в string
        /// </summary>
        /// <param name="typeUAVRes"></param>
        /// <returns></returns>
        private string ConvertTypeDroneUAVRes(TypeUAVRes typeUAVRes)
        {
            string sType = string.Empty;

            switch (typeUAVRes)
            {
                case TypeUAVRes.Ocusinc:
                    sType = "Mavic 2";
                    break;

                case TypeUAVRes.G3:
                    sType = "3G";
                    break;

                case TypeUAVRes.Lightbridge:
                    sType = "Phantom 4";
                    break;

                case TypeUAVRes.WiFi:
                    sType = "Mavic Air";
                    break;

                case TypeUAVRes.Unknown:
                    sType = "Unknown";
                    break;

                default:
                    sType = string.Empty;
                    break;
            }

            return sType;
        }

        /// Определить имя таблицы
        /// </summary>
        /// <returns></returns>
        private NameTable FindNameTableUAVResArchive()
        {
            switch (NameTable)
            {
                case UAVResArchive.UAVRes:
                    return KirasaModelsDBLib.NameTable.TableUAVRes;
                case UAVResArchive.UAVResArchive:
                    return KirasaModelsDBLib.NameTable.TableUAVResArchive;
                default:
                    break;
            }
            return KirasaModelsDBLib.NameTable.TableUAVRes;
        }

        /// <summary>
        /// Определить тип таблицы
        /// </summary>
        /// <param name="sr"></param>
        /// <returns></returns>
        private AbstractCommonTable FindTypeUAVResArchive(TableUAVBase table)
        {
            switch (NameTable)
            {
                case UAVResArchive.UAVRes:
                    return table.ToUAVRes();
                case UAVResArchive.UAVResArchive:
                    return table.ToUAVResArchive();
                default:
                    break;
            }
            return null;
        }

        /// <summary>
        /// Обновить время наблюдения (TrackTime)
        /// </summary>
        /// <param name="listUAVRes"></param>
        public DateTime UpdateTrackTimeUAVResArchive(List<TableTrajectory> listUAVTrajectory)
        {
            DateTime TrackTime = new DateTime();
            
            try
            {
                if (listUAVTrajectory.Count > 0)
                {
                    TableTrajectory TrackTimeFirst = listUAVTrajectory.First();
                    TableTrajectory trackTimeLast = listUAVTrajectory.Last();

                    TimeSpan timeSpan = trackTimeLast.Time - TrackTimeFirst.Time;
                    TrackTime = Convert.ToDateTime(timeSpan.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }

            return TrackTime;
        }
    }
}
