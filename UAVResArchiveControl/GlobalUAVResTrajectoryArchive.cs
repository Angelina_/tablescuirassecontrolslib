﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace UAVResArchiveControl
{
    public class GlobalUAVResTrajectoryArchive
    {
        public ObservableCollection<TableTrajectory> CollectionUAVTrajectoryArchive { get; set; }

        public GlobalUAVResTrajectoryArchive()
        {
            try
            {
                CollectionUAVTrajectoryArchive = new ObservableCollection<TableTrajectory> { };

                //CollectionUAVTrajectoryArchive = new ObservableCollection<TableTrajectory>
                //{
                //    new TableTrajectory
                //    {
                //        Id = 1,
                //        Num = 1,
                //        FrequencyKHz = 55455.9,
                //        BandKHz = 34.6F,
                //        Coordinates = new Coord() { Altitude = 1, Latitude = -53.123445, Longitude = 27.456459 },
                //        Elevation = 500,
                //        Time = DateTime.Now
                //    },
                //    new TableTrajectory
                //    {
                //        Id = 2,
                //        Num = 2,
                //        FrequencyKHz = 88895.788,
                //        BandKHz = 67.6F,
                //        Coordinates = new Coord() { Altitude = 211, Latitude = 56.14577777884, Longitude = -27.845869 },
                //        Elevation = 500,
                //        Time = DateTime.Now
                //    },
                //     new TableTrajectory
                //    {
                //        Id = 3,
                //        Num = 1,
                //        FrequencyKHz = 55555.5,
                //        BandKHz = 55.5F,
                //        Coordinates = new Coord() { Altitude = 550, Latitude = 52.14577777884, Longitude = -26.845869 },
                //        Elevation = 450,
                //        Time = DateTime.Now
                //    }
                //};
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
