﻿using KirasaModelsDBLib;
using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace UAVResArchiveControl
{
    public class GlobalUAVResArchive
    {
        public ObservableCollection<TableUAVBase> CollectionUAVResArchive { get; set; }

        public GlobalUAVResArchive()
        {
            try
            {
                CollectionUAVResArchive = new ObservableCollection<TableUAVBase> { };

                //CollectionUAVResArchive = new ObservableCollection<TableUAVBase>
                //{
                //    new TableUAVBase
                //    {
                //        Id = 1,
                //        IsActive = false,
                //        Type = TypeUAVRes.Ocusinc,
                //        Note = "Jk8 dflk okl"
                //    },
                //    new TableUAVBase
                //    {
                //        Id = 2,
                //        IsActive = true,
                //        Type = TypeUAVRes.Lightbridge,
                //        Note = "jkdsgj 878"
                //    }
                //};
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
