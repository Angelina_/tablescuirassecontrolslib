﻿using System;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace ValuesCorrectLib
{
    public class UAVResArchiveNoteEditor : PropertyEditor
    {
        public UAVResArchiveNoteEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/UAVResArchiveControl;component/Themes/PropertyGridEditorUAVResArchive.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["UAVResArchiveNoteEditorKey"];
        }
    }

    public class UAVResArchiveTrackTimeEditor : PropertyEditor
    {
        public UAVResArchiveTrackTimeEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/UAVResArchiveControl;component/Themes/PropertyGridEditorUAVResArchive.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["UAVResArchiveTrackTimeEditorKey"];
        }

    }
    public class UAVResArchiveTypeEditor : PropertyEditor
    {
        public UAVResArchiveTypeEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/UAVResArchiveControl;component/Themes/PropertyGridEditorUAVResArchive.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["UAVResArchiveTypeEditorKey"];
        }
    }
}
