﻿using System.Collections.ObjectModel;

namespace ValuesCorrectLib
{
    public class ValuesNumPoint : ObservableCollection<int>
    {
        public ValuesNumPoint()
        {
            Add(1);
            Add(2);
            Add(3); 
            Add(4);
        }

    }
}
