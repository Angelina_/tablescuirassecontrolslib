﻿using System;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace ValuesCorrectLib
{
    public class OtherPointsCoordinatesEditor : PropertyEditor
    {
        public OtherPointsCoordinatesEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/OtherPointsControl;component/Themes/PropertyGridEditorOtherPoints.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["OtherPointsCoordinatesEditorKey"];
        }
    }

    public class OtherPointsTypeEditor : PropertyEditor
    {
        public OtherPointsTypeEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/OtherPointsControl;component/Themes/PropertyGridEditorOtherPoints.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource["OtherPointsTypeEditorKey"];
        }
    }

}
