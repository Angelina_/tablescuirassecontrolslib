﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace ValuesCorrectLib
{
    public class ConstValues
    {
        #region Const Freqs for ToggleButton (SuppressFWS)
        public const double dFreqIridium = 1621250.0F;
        public const double dFreqInmarsat = 1542000.0F;
        public const double dFreq2_4 = 2442500.0F;
        public const double dFreq5_8 = 5800000.0F;
        public const double dFreqGlobalStar = 2491750.0F;
        public const double dFreq433 = 434500.0F;
        public const double dFreq868 = 866500.0F;
        public const double dFreq920 = 915000.0F;
        public const double dFreqNAV_L1 = 1575420.0F;
        public const double dFreqNAV_L2 = 1602000.0F;
        //public const double dFreqNAV_L1 = 1575420.0F;
        //public const double dFreqNAV_L2 = 1227600.0F;
        //public const double dFreqNAV_L3 = 1176450.0F;
        #endregion

        #region Const Level
        public const Int16 constLevel = -130;
        public const Int16 constThreshold = -80;
        #endregion
    }


}
