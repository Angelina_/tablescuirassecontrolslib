﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValuesCorrectLib
{
    #region Сообщения об ошибках
    public struct SMessageError
    {
        public static string mesErr;
        public static string mesErrValBandwidth;
        public static string mesErrEnterValues;

        public static void InitSMessageError()
        {
            mesErr = "Ошибка!";
            mesErrValBandwidth = "Значение ширины полосы задается от 0 до 1000!";
            mesErrEnterValues = "Введите значения!";
        }
    }
    #endregion

    #region Сообщения
    public struct SMessages
    {
        public static string mesMessage;
        public static string mesValuesMaxMin;
        public static string mesNumReceivingPoint;
        public static string mesAlreadyExists;
        public static string mesPassword;


        public static void InitSMessages()
        {
            mesMessage = "Сообщение!";
            mesValuesMaxMin = "Значение поля 'F мин.' должно быть меньше значения поля 'F макс.'!";
            mesNumReceivingPoint = "Пункт № ";
            mesAlreadyExists = " уже существует!";
            mesPassword = "Неверный пароль!";
        }
    }
    #endregion

    #region Значения
    public struct SMeaning
    {
        public static string meaningAddRecord;
        public static string meaningChangeRecord;

        public static string meaningHz;
        public static string meaningkHz;
        public static string meaningMHz;

        public static string meaningmks;
        public static string meaningms;
        
        public static string meaningCoord;

        public static string meaningSearch;
        public static string meaningLocation;

        public static string meaningTypeOtherPoints;

        public static void InitSMeaning()
        {
            meaningAddRecord = "Добавить запись";
            meaningChangeRecord = "Изменить запись";

            meaningHz = "Гц";
            meaningkHz = "кГц";
            meaningMHz = "МГц";

            meaningmks = "мкс";
            meaningms = "мс";

            meaningCoord = "Координаты";

            meaningSearch = "ПОИСК";
            meaningLocation = "МЕСТООПРЕДЕЛЕНИЕ";

            meaningTypeOtherPoints = "Гроза-С";
        }
    }
    #endregion

    #region Wnd CombineTrack
    public struct SWndCombineTrack
    {
        public static string labelTypeSource;
        public static string labelUnionTracks;

        public static void InitWndCombineTrack()
        {
            labelTypeSource = "Выберите тип источника:";
            labelUnionTracks = "Объединить траектории";
        }
    }
    #endregion

    #region Заголовки таблиц для отчетов (текстовых файлов .doc, .xls, .txt)
    public struct SHeaders
    {
        public static string headerLatLon;
        public static string headerAlt;
        public static string headerFreqMin;
        public static string headerFreqMax;
        public static string headerNote;
        public static string headerDeltaF;
        public static string headerNum;
        public static string headerFreq;
        public static string headerCount;
        public static string headerTimeError;


        public static void InitSHeader()
        {
            headerLatLon = "Шир.,°  Долг.,°";
            headerAlt = "Выс., м";
            headerFreqMin = "F мин., кГц";
            headerFreqMax = "F макс., кГц";
            headerNote = "Примечание";
            headerDeltaF = "Δf, кГц";
            headerNum = "№";
            headerFreq = "F, кГц";
            headerCount = "Кол-во";
            headerTimeError = "Ошибка вр. изм., мкс";
        }
    }
    #endregion
}
