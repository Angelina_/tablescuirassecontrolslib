﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Xml;
using System.Xml.Linq;

namespace ValuesCorrectLib
{
    public class TranslatorTables
    {
        public static void ChangeLanguagePropertyGrid(DllCuirassemProperties.Models.Languages language, System.Windows.Controls.WpfPropertyGrid.PropertyGrid propertyGrid)
        {
            LoadDictionary(language);

            foreach (var prop in propertyGrid.Properties)
            {
                SetNamePropery(prop.Name, propertyGrid);
            }

            foreach (var category in propertyGrid.Categories)
            {
                SetNameCategory(category.Name, propertyGrid);
            }
        }

        private static void SetNamePropery(string nameProperty, System.Windows.Controls.WpfPropertyGrid.PropertyGrid propertyGrid)
        {
            try
            {
                propertyGrid.Properties[nameProperty].DisplayName = TranslateDic[nameProperty];
            }
            catch (Exception) { }
        }

        private static void SetNameCategory(string nameCategory, System.Windows.Controls.WpfPropertyGrid.PropertyGrid propertyGrid)
        {
            try
            {
                propertyGrid.Categories.First(t => t.Name == nameCategory).HeaderCategoryName = TranslateDic[nameCategory];
            }
            catch (Exception) { }
        }

        static Dictionary<string, string> TranslateDic;
        public static void LoadDictionary(DllCuirassemProperties.Models.Languages language)
        {
            XmlDocument xDoc = new XmlDocument();

            // Для отладки --------------------------------------------------------------------
            //string str = (Directory.GetCurrentDirectory());
            // -------------------------------------------------------------------- Для отладки

            var translation = Properties.Resources.TranslatorTablesCuirasse;
            xDoc.LoadXml(translation);


            //if (File.Exists(Directory.GetCurrentDirectory() + "\\Languages" + "\\TranslatorTables" + "\\TranslatorTablesCuirasse.xml"))
            //    xDoc.Load(Directory.GetCurrentDirectory() + "\\Languages" + "\\TranslatorTables" + "\\TranslatorTablesCuirasse.xml");
            //else
            //{
            //    switch (language)
            //    {
            //        case DllCuirassemProperties.Models.Languages.RU:
            //            // MessageBox.Show("Файл XMLTranslation.xml не найден!", "Error!");
            //            break;
            //        case DllCuirassemProperties.Models.Languages.EN:
            //            //MessageBox.Show("File XMLTranslation.xml not found!", "Error!");
            //            break;
            //        default:
            //            // MessageBox.Show("Файл XMLTranslation.xml не найден!", "Error!");
            //            break;
            //    }
            //    return;
            //}

            TranslateDic = new Dictionary<string, string>();

            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == language.ToString())
                            {
                                if (!TranslateDic.ContainsKey(attr.Value))
                                    TranslateDic.Add(attr.Value, childnode.InnerText);
                            }
                        }
                    }
                }
            }

            if (TranslateDic.Count > 0 && TranslateDic != null)
            {
                FunctionsTranslate.RenameMeaning(TranslateDic);
                FunctionsTranslate.RenameMessages(TranslateDic);
                FunctionsTranslate.RenameHeaders(TranslateDic);
                FunctionsTranslate.RenameWndCombineTrack(TranslateDic);
            }
        }

        public static void ChangeLanguageWndCombineTrack(DllCuirassemProperties.Models.Languages language)
        {
            LoadDictionary(language);
        }
    }
}
