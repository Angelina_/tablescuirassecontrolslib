﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ValuesCorrectLib
{
    public class CorrectParams
    {
        public static void IsCorrectFreq(TableTrajectory UAVTrajectory)
        {
            if (UAVTrajectory.FrequencyKHz < 30.0F || UAVTrajectory.FrequencyKHz > 6000.0F) { UAVTrajectory.FrequencyKHz = 30.0F; }
        }

        public static void IsCorrectBand(TableUAVTrajectory UAVTrajectory)
        {
            if (UAVTrajectory.BandKHz < 0.0F || UAVTrajectory.BandKHz > 1000.0F) { UAVTrajectory.BandKHz = 0.0F; }
        }

        public static void IsCorrectElevation(TableUAVTrajectory UAVTrajectory)
        {
            if (UAVTrajectory.Elevation < 0.0F || UAVTrajectory.Elevation > 5000.0F) { UAVTrajectory.Elevation = 100.0F; }
        }

        public static void IsCorrectAntenna(TableReceivingPoint ReceivingPoint)
        {
            if (ReceivingPoint.Antenna < 0.0F || ReceivingPoint.Antenna > 5000.0F) { ReceivingPoint.Antenna = 100.0F; }
        }

        /// <summary>
        /// Проверка на правильность ввода значений:  
        /// если Fmin меньше 25 или больше 6000, Fmin = 25;
        /// если Fmax меньше 25 или больше 6000, Fmax = 6000;
        /// </summary>
        /// <param name="suppressFHSS">Fmin, Fmax</param>
        public static void IsCorrectMinMax(TableFreqRanges freqRanges)
        {
            if (freqRanges.FreqMinKHz < 25.0F || freqRanges.FreqMinKHz > 6000.0F) { freqRanges.FreqMinKHz = 25.0F; }
            if (freqRanges.FreqMaxKHz < 25.0F || freqRanges.FreqMaxKHz > 6000.0F) { freqRanges.FreqMaxKHz = 6000.0F; }
        }

        /// <summary>
        /// Проверка на корректность ввода значений частоты
        /// </summary>
        /// <param name="iFreqMin"> начальное значение частоты </param>
        /// <param name="iFreqMax"> конечное значение частоты </param>
        /// <returns> true - успешно, false - нет </returns>
        //private static bool IsCorrectFreqMinMax(int iFreqMin, int iFreqMax)
        public static bool IsCorrectFreqMinMax(double dFreqMin, double dFreqMax)
        {
            bool bCorrect = true;

            if (dFreqMin >= dFreqMax)
            {
                MessageBox.Show(SMessages.mesValuesMaxMin, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);

                bCorrect = false;
            }

            return bCorrect;
        }

        /// <summary>
        /// Проверка возможноси добавления номера локального пункта
        /// </summary>
        /// <param name="collectionTemp"> локальные пункты в таблице </param>
        /// <param name="ReceivingPointWindow">добавляемый пункт</param>
        /// <returns> true - можно добавить, false - нельзя (пункт с такм номером уже есть) </returns>
        public static bool IsAddReceivingPoint(ObservableCollection<TableReceivingPoint> collectionTemp, TableReceivingPoint ReceivingPointWindow)
        {
            bool bAdd= true;

            int ind = collectionTemp.ToList().FindIndex(x => x.Id == ReceivingPointWindow.Id);
            if (ind != -1)
            {
                MessageBox.Show(SMessages.mesNumReceivingPoint + ReceivingPointWindow.Id + SMessages.mesAlreadyExists, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);

                bAdd = false;
            }

            return bAdd;
        }
    }
}
