﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TableEvents;

namespace SignalsUAVControl
{
    public partial class UserControlSignalsUAV : UserControl
    {
        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="SignalsUAV"></param>
        public void UpdateSignalsUAV(List<TableSignalsUAV> SignalsUAV)
        {
            try
            {
                if (SignalsUAV == null)
                    return;

                tableViewModel.CollectionSignalsUAV.Clear();

                for (int i = 0; i < SignalsUAV.Count; i++)
                {
                    tableViewModel.CollectionSignalsUAV.Add(SignalsUAV[i]);
                }

                AddEmptyRows();
                GC.Collect(1, GCCollectionMode.Optimized);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listSignalsUAV"></param>
        public void AddSignalsUAV(List<TableSignalsUAV> listSignalsUAV)
        {
            try
            {
                DeleteEmptyRows();

                for (int i = 0; i < listSignalsUAV.Count; i++)
                {
                    int ind = ((GlobalSignalsUAV)DgvSignalsUAV.DataContext).CollectionSignalsUAV.ToList().FindIndex(x => x.Id == listSignalsUAV[i].Id);
                    if (ind != -1)
                    {
                        tableViewModel.CollectionSignalsUAV[ind] = listSignalsUAV[i];
                    }
                    else
                    {
                        tableViewModel.CollectionSignalsUAV.Add(listSignalsUAV[i]);
                    }
                }

                AddEmptyRows();

                DgvSignalsUAV.SelectedIndex = PropNumUAV.SelectedNumSignalsUAV;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteSignalsUAV(TableSignalsUAV tableSignalsUAV)
        {
            try
            {
                int index = tableViewModel.CollectionSignalsUAV.ToList().FindIndex(x => x.Id == tableSignalsUAV.Id);
                if (index != -1)
                {
                    tableViewModel.CollectionSignalsUAV.RemoveAt(index);
                }

                AddEmptyRows();

                DgvSignalsUAV.SelectedIndex = PropNumUAV.SelectedNumSignalsUAV;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        ///// <summary>
        ///// Добавить в таблицу пустые строки
        ///// </summary>
        //private void AddEmptyRows()
        //{
        //    try
        //    {
        //        int сountRowsAll = DgvSignalsUAV.Items.Count; // количество всех строк в таблице
        //        double hs = 23; // высота строки
        //        double ah = DgvSignalsUAV.ActualHeight; // визуализированная высота dataGrid
        //        double chh = DgvSignalsUAV.ColumnHeaderHeight; // высота заголовка

        //        int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

        //        int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
        //        int index = -1;
        //        for (int i = 0; i < count; i++)
        //        {
        //            // Удалить пустые строки в dgv
        //            index = ((GlobalSignalsUAV)DgvSignalsUAV.DataContext).CollectionSignalsUAV.ToList().FindIndex(x => x.Id < 0);
        //            if (index != -1)
        //            {
        //                ((GlobalSignalsUAV)DgvSignalsUAV.DataContext).CollectionSignalsUAV.RemoveAt(index);
        //            }
        //        }

        //        List<TableSignalsUAV> list = new List<TableSignalsUAV>();
        //        for (int i = 0; i < countRows - сountRowsAll; i++)
        //        {
        //            TableSignalsUAV strTable = new TableSignalsUAV
        //            {
        //                Id = -2,
        //                Correlation = false,
        //                BandKHz = -2,
        //                Type = (TypeUAVRes)255,
        //                TypeM = (TypeM)255,
        //                TypeL = (TypeL)255,
        //                System = (TypeSystem)255
        //            };

        //            list.Add(strTable);
        //        }

        //        for (int i = 0; i < list.Count; i++)
        //        {
        //            ((GlobalSignalsUAV)DgvSignalsUAV.DataContext).CollectionSignalsUAV.Add(list[i]);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message.ToString());
        //    }
        //}

        /// <summary>
        /// Добавление пустых строк в таблицу
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                //paramSatellite.Items.Refresh(); //надо ли?
                int сountRowsAll = DgvSignalsUAV.Items.Count; // количество имеющихся строк в таблице
                double hs = 23; // высота строки
                double ah = DgvSignalsUAV.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvSignalsUAV.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++) //мб переставить циклы местами
                {
                    index = tableViewModel.CollectionSignalsUAV.ToList().FindIndex(x => x.Id < 0);
                    //index = DgvSignalsUAV.CollectionSignalsUAV.ToList().FindIndex(x => x.PRN < 0);
                    if (index != -1)
                    {
                        tableViewModel.CollectionSignalsUAV.RemoveAt(index);
                        //DgvSignalsUAV.FullParams.RemoveAt(index);
                    }
                }
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    tableViewModel.CollectionSignalsUAV.Add(new TableSignalsUAV
                                                                {
                                                                    Id = -2,
                                                                    Correlation = false,
                                                                    BandKHz = -2,
                                                                    Type = (TypeUAVRes)255,
                                                                    TypeM = (TypeM)255,
                                                                    TypeL = (TypeL)255,
                                                                    System = (TypeSystem)255
                                                                });
                    //DgvSignalsUAV.FullParams.Add(new Satellite());
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = tableViewModel.CollectionSignalsUAV.Count(s => s.Id < 0);
                int countAllRows = tableViewModel.CollectionSignalsUAV.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    tableViewModel.CollectionSignalsUAV.RemoveAt(iCount);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (this.tableViewModel.SelectedSignal.Id == -2)
                    return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }

            return true;
        }
    }
}
