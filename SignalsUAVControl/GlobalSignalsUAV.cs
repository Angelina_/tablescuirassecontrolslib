﻿using KirasaModelsDBLib;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Runtime.CompilerServices;

namespace SignalsUAVControl
{
    public class GlobalSignalsUAV : DependencyObject, INotifyPropertyChanged
    {
        public ObservableCollection<TableSignalsUAV> CollectionSignalsUAV { get; set; }

        public TableSignalsUAV SelectedSignal { get; set; }

        public GlobalSignalsUAV()
        {
            try
            {
                CollectionSignalsUAV = new ObservableCollection<TableSignalsUAV> { };

                #region Test
                //CollectionSignalsUAV = new ObservableCollection<TableSignalsUAV>
                //{
                //    new TableSignalsUAV
                //    {
                //        Id = 1,
                //        Correlation = false,
                //        FrequencyKHz = 88895.788,
                //        BandKHz = 67.6F,
                //        Type = TypeUAVRes.Ocusinc,
                //        TypeM = TypeM.Drone,
                //        TypeL = TypeL.Enemy,
                //        TimeStart = DateTime.Now,
                //        TimeStop = DateTime.Now,
                //        System = TypeSystem.CP_CuirasseM
                //    },
                //    new TableSignalsUAV
                //    {
                //        Id = 2,
                //        Correlation = true,
                //        FrequencyKHz = 44495.558,
                //        BandKHz = 24.7F,
                //        Type = TypeUAVRes.Lightbridge,
                //        TypeM = TypeM.Unknown,
                //        TypeL = TypeL.Friend,
                //        TimeStart = DateTime.Now,
                //        TimeStop = DateTime.Now,
                //        System = TypeSystem.Martsinovich
                //    }
                //};

                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
    }
}
