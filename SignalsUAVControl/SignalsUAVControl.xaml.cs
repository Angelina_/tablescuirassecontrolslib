﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TableEvents;

namespace SignalsUAVControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlSignalsUAV : UserControl
    {
        #region Events
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableSignalsUAV> OnGetTypeM = (object sender, TableSignalsUAV data) => { };
        public event EventHandler<TableSignalsUAV> OnGetTypeL = (object sender, TableSignalsUAV data) => { };
        #endregion

        #region Properties
        public NameTable NameTableSignalsUAV { get; } = NameTable.TableSignalsUAV;
        private GlobalSignalsUAV tableViewModel = new GlobalSignalsUAV();
        #endregion

        public UserControlSignalsUAV()
        {
            InitializeComponent();
            this.DataContext = this.tableViewModel;
            
            //DgvSignalsUAV.DataContext = new GlobalSignalsUAV();
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.tableViewModel.SelectedSignal != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TableSignalsUAV tableSignalsUAV = new TableSignalsUAV
                    {
                        Id = this.tableViewModel.SelectedSignal.Id,
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent(tableSignalsUAV));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, NameTable.TableSignalsUAV);
            }
            catch { }
        }

        private void ButtonGetTypeM_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.tableViewModel.SelectedSignal != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    // Событие Запрос Тип М
                    OnGetTypeM(this, this.tableViewModel.SelectedSignal);
                    //OnGetTypeM(this, new TableEvent((TableSignalsUAV)DgvSignalsUAV.SelectedItem));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private void ButtonGetTypeL_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.tableViewModel.SelectedSignal != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    // Событие Запрос Тип Л
                    OnGetTypeL(this, this.tableViewModel.SelectedSignal);
                    //OnGetTypeL(this, new TableEvent((TableSignalsUAV)DgvSignalsUAV.SelectedItem));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private void DgvSignalUAV_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void DgvSignalUAV_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (this.tableViewModel.SelectedSignal == null) return;

                if (this.tableViewModel.SelectedSignal.Id > -1)
                {
                    if (this.tableViewModel.SelectedSignal.Id != PropNumUAV.SelectedIdSignalsUAV)
                    {
                        int ind = tableViewModel.CollectionSignalsUAV.ToList().FindIndex(x => x.Id == this.tableViewModel.SelectedSignal.Id);
                        if (ind != -1)
                        {
                            PropNumUAV.SelectedNumSignalsUAV = ind;
                            PropNumUAV.SelectedIdSignalsUAV = this.tableViewModel.SelectedSignal.Id;
                        }
                    }
                }
                else
                {
                    PropNumUAV.SelectedNumSignalsUAV = 0;
                    PropNumUAV.SelectedIdSignalsUAV = 0;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.tableViewModel.SelectedSignal.Id > -1)
                {
                    if (this.tableViewModel.SelectedSignal.Correlation)
                    {
                        this.tableViewModel.SelectedSignal.Correlation = false;
                    }
                    else
                    {
                        this.tableViewModel.SelectedSignal.Correlation = true;
                    }

                    // Событие изменения одной записи
                    OnChangeRecord(this, new TableEvent(this.tableViewModel.SelectedSignal));
                }
                else
                {
                    CheckBox chbIsChecked = sender as CheckBox;
                    chbIsChecked.IsChecked = false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }
    }
}
