﻿using ClientDataBase;
using InheritorsEventArgs;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using ValuesCorrectLib;

namespace ProjectTablesControlTEST
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        public MainWindow()
        {
            InitializeComponent();

            InitTables();

            SetLanguageTables(basicProperties.Local.Common.Language);

            TranslatorTables.LoadDictionary(basicProperties.Local.Common.Language);

            //MainWnd.Owner = this;
        }

       
      
    }
}
