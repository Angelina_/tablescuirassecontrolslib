﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableEvents;

namespace ProjectTablesControlTEST
{
    public partial class MainWindow
    {
        public void InitTables()
        {
            // Таблица ИРИ БПЛА
            ucUAVRes.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucUAVRes.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucUAVRes.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);

            // Таблица Локальные пункты приема (ЛПП)
            ucLocalPoints.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucLocalPoints.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucLocalPoints.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucLocalPoints.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucLocalPoints.OnIsWindowPropertyOpen += new EventHandler<ReceivingPointControl.ReceivingPointProperty>(UcLocalPoints_OnIsWindowPropertyOpen);

            // Таблица Удаленные пункты приема(УПП)
            ucRemotePoints.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucRemotePoints.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucRemotePoints.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucRemotePoints.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucRemotePoints.OnIsWindowPropertyOpen += new EventHandler<ReceivingPointControl.ReceivingPointProperty>(UcRemotePoints_OnIsWindowPropertyOpen);

            // Таблица Известные частоты (ИЧ)
            ucFreqKnown.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucFreqKnown.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucFreqKnown.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucFreqKnown.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucFreqKnown.OnIsWindowPropertyOpen += new EventHandler<FreqRangesControl.FreqRangesProperty>( UcFreqKnown_OnIsWindowPropertyOpen);

            // Таблица Диапазоны радиоразведки (ДРР)
            ucFreqRangesRecon.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucFreqRangesRecon.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucFreqRangesRecon.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucFreqRangesRecon.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucFreqRangesRecon.OnIsWindowPropertyOpen += new EventHandler<FreqRangesControl.FreqRangesProperty>(UcFreqRangesRecon_OnIsWindowPropertyOpen);

            // Таблица Аэроскоп БПЛА (А БПЛА)
            ucAeroscope.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucAeroscope.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
        }

        private void UcUAVRes_OnChangeRecord(object sender, TableEvent e)
        {
            throw new NotImplementedException();
        }
    }
}
