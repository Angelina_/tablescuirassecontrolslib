﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableEvents;

namespace ProjectTablesControlTEST
{
    public partial class MainWindow
    {
        // ИРИ БПЛА
        public List<TableUAVBase> lUAVRes = new List<TableUAVBase>();
        // Траектория ИРИ БПЛА
        public List<TableTrajectory> lUAVTrajectory = new List<TableTrajectory>();
        // Локальные пункты приема (ЛПП)
        public List<TableReceivingPoint> lLocalPoints = new List<TableReceivingPoint>();
        // Удаленные пункты приема(УПП)
        public List<TableReceivingPoint> lRemotePoints = new List<TableReceivingPoint>();
        // Известные частоты (ИЧ)
        public List<TableFreqRanges> lFreqKnown = new List<TableFreqRanges>();
        // Диапазоны радиоразведки (ДРР)
        public List<TableFreqRanges> lFreqRangesRecon = new List<TableFreqRanges>();
        // Аэроскоп БПЛА
        public List<TableAeroscope> lAeroscope = new List<TableAeroscope>();
        // Траектория А БПЛА
        public List<TableAeroscopeTrajectory> lATrajectory = new List<TableAeroscopeTrajectory>();

        private int selectedUAVRes = -2;
        private int selectedAeroscope = -2;

        // Добавить запись
        private void OnAddRecord(object sender, TableEvent e)
        {
            if (clientDB != null)
            {
                clientDB.Tables[e.NameTable].Add(e.Record);
            }
        }

        // Удалить все записи
        private void OnClearRecords(object sender, NameTable nameTable)
        {
            if (clientDB != null)
            {
                clientDB.Tables[nameTable].Clear();
            }
        }

        // Удалить запись
        private void OnDeleteRecord(object sender, TableEvent e)
        {
            if (clientDB != null)
            {
                clientDB.Tables[e.NameTable].Delete(e.Record);
            }
        }

        // Изменить запись
        private void OnChangeRecord(object sender, TableEvent e)
        {
            if (clientDB != null)
            {
                clientDB.Tables[e.NameTable].Change(e.Record);
            }
        }
    }
}
