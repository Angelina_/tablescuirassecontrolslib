﻿using ClientDataBase;
using InheritorsEventArgs;
using System;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Media;

namespace ProjectTablesControlTEST
{
    public partial class MainWindow
    {
        private void HandlerErrorDataBase(object sender, OperationTableEventArgs eventArgs)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                TextBoxMessage.Foreground = Brushes.Red;
                TextBoxMessage.AppendText(eventArgs.GetMessage);
            });
        }

        private void HandlerUpData(object sender, DataEventArgs eventArgs)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                TextBoxMessage.Foreground = Brushes.White;
                TextBoxMessage.AppendText($"{DateTime.Now}\nLoad data from Db. Tables {eventArgs.Name.ToString()} count records - {eventArgs.AbstractData.ListRecords.Count} \n");
            });
        }

        private void HandlerDisconnect(object sender, ClientEventArgs eventArgs)
        {
            if (eventArgs.GetMessage != "")
            {
                DbControlConnection.ShowDisconnect();
            }
            clientDB = null;
        }

        private void HandlerConnect(object sender, ClientEventArgs e)
        {
            DbControlConnection.ShowConnect();
            LoadTables();
        }

        private void DbConnection_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (clientDB != null)
                    clientDB.Disconnect();
                else
                {
                    clientDB = new ClientDB(this.Name, endPoint);
                    InitClientDB();
                    clientDB.ConnectAsync();
                }
            }
            catch (ClientDataBase.ExceptionClient exceptClient)
            {
                HandlerDisconnect(this, null);
                MessageBox.Show(exceptClient.Message);
            }
        }
    }
}
