﻿using InheritorsEventArgs;
using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using TableEvents;

namespace ProjectTablesControlTEST
{
    public partial class MainWindow
    {
        private void OnUpTable_TableUAVRes(object sender, TableEventArgs<TableUAVRes> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lUAVRes = (from t in e.Table let a = t as TableUAVBase select a).ToList();
                if (lUAVRes.Count == 0)
                    ucUAVRes.UpdateUAVRes(lUAVRes);
                else
                {
                    ucUAVRes.AddUAVRes(lUAVRes);
                    ucUAVRes.ColorRowState(lUAVRes);
                }

                //Console.WriteLine(PropNumUAV.SelectedNumUAVRes.ToString() + "\n");

            });
        }

        private void OnUpTable_TableUAVTrajectory(object sender, TableEventArgs<TableUAVTrajectory> e)
        {
            //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            //{
            //    lUAVTrajectory = e.Table;
            //    List<TableUAVTrajectory> list = lUAVTrajectory.Where(x => x.TableUAVResId == PropNumUAV.SelectedNumUAVRes).ToList();

            //    if (lUAVTrajectory.Count == 0)
            //        ucUAVRes.UpdateUAVTrajectory(lUAVTrajectory);
            //    else
            //        ucUAVRes.AddUAVTrajectory(list);
            //});
        }
        private void OnDeleteRecord_TableUAVRes(object sender, TableUAVRes e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                int ind = lUAVRes.FindIndex(x => x.Id == e.Id);
                if (ind != -1)
                {
                    lUAVRes.RemoveAt(ind);
                    ucUAVRes.DeleteUAVRes(e);
                }
            });
        }

        private void UcUAVRes_OnSelectedRow(object sender, SelectedRowEvents e)
        {
            //if (e.Id > -1)
            //{
            //    selectedUAVRes = e.Id;

            //    List<TableUAVTrajectory> list = lUAVTrajectory.Where(x => x.TableUAVResId == PropNumUAV.SelectedNumUAVRes).ToList();
            //    ucUAVRes.UpdateUAVTrajectory(list);
            //}
        }

        private void UcAeroscope_OnSelectedRow(object sender, SelectedRowEvents e)
        {
            if (e.Id > -1)
            {
                selectedAeroscope = e.Id;

                List<TableAeroscopeTrajectory> list = lATrajectory.Where(x => x.SerialNumber == PropNumUAV.SelectedSerialNumAeroscope).ToList();
                //ucAeroscope.UpdateATrajectory(list);
            }
        }

        private void OnUpTable_TableRemotePoints(object sender, TableEventArgs<TableRemotePoints> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lRemotePoints = (from t in e.Table let a = t as TableReceivingPoint select a).ToList();
                ucRemotePoints.UpdateReceivingPoint(lRemotePoints);
            });
        }

        private void OnUpTable_TableLocalPoints(object sender, TableEventArgs<TableLocalPoints> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lLocalPoints = (from t in e.Table let a = t as TableReceivingPoint select a).ToList();
                ucLocalPoints.UpdateReceivingPoint(lLocalPoints);
            });
        }

        private void OnUpTable_TableFreqRangesRecon(object sender, TableEventArgs<TableFreqRangesRecon> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lFreqRangesRecon = (from t in e.Table let a = t as TableFreqRanges select a).ToList();
                ucFreqRangesRecon.UpdateFreqRanges(lFreqRangesRecon);
            });
        }

        private void OnUpTable_TableFreqKnown(object sender, TableEventArgs<TableFreqKnown> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lFreqKnown = (from t in e.Table let a = t as TableFreqRanges select a).ToList();
                ucFreqKnown.UpdateFreqRanges(lFreqKnown);
            });
        }

        private void OnUpTable_TableAeroscope(object sender, TableEventArgs<TableAeroscope> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lAeroscope = e.Table;
                if (lAeroscope.Count == 0)
                    ucAeroscope.UpdateAeroscope(lAeroscope);
                else
                {
                    ucAeroscope.AddAeroscope(lAeroscope);
                    //ucAeroscope.ColorRowState(lAeroscope);
                }
            });
        }

        private void OnUpTable_TableAeroscopeTrajectory(object sender, TableEventArgs<TableAeroscopeTrajectory> e)
        {
            //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            //{
            //    lATrajectory = e.Table;
            //    List<TableAeroscopeTrajectory> list = lATrajectory.Where(x => x.SerialNumber == PropNumUAV.SelectedSerialNumAeroscope).ToList();

            //    if (lATrajectory.Count == 0)
            //        ucAeroscope.UpdateATrajectory(lATrajectory);
            //    else
            //        ucAeroscope.AddATrajectory(list);

            //});
        }

        private async void LoadTables()
        {
            try
            {
                lUAVRes = await clientDB.Tables[NameTable.TableUAVRes].LoadAsync<TableUAVBase>();
                ucUAVRes.UpdateUAVRes(lUAVRes);

                //lUAVTrajectory = await clientDB.Tables[NameTable.TableUAVTrajectory].LoadAsync<TableUAVTrajectory>();
                //List<TableUAVTrajectory> list = lUAVTrajectory.Where(x => x.TableUAVResId == PropNumUAV.SelectedNumUAVRes).ToList();
                //ucUAVRes.UpdateUAVTrajectory(list);

                lAeroscope = await clientDB.Tables[NameTable.TableАeroscope].LoadAsync<TableAeroscope>();
                ucAeroscope.UpdateAeroscope(lAeroscope);

                lATrajectory = await clientDB.Tables[NameTable.TableАeroscopeTrajectory].LoadAsync<TableAeroscopeTrajectory>();
                List<TableAeroscopeTrajectory> listA = lATrajectory.Where(x => x.SerialNumber == PropNumUAV.SelectedSerialNumAeroscope).ToList();
                //ucAeroscope.UpdateATrajectory(listA);

                lRemotePoints = await clientDB.Tables[NameTable.TableRemotePoints].LoadAsync<TableReceivingPoint>();
                ucRemotePoints.UpdateReceivingPoint(lRemotePoints);

                lLocalPoints = await clientDB.Tables[NameTable.TableLocalPoints].LoadAsync<TableReceivingPoint>();
                ucLocalPoints.UpdateReceivingPoint(lLocalPoints);

                lFreqKnown = await clientDB.Tables[NameTable.TableFreqKnown].LoadAsync<TableFreqRanges>();
                ucFreqKnown.UpdateFreqRanges(lFreqKnown);

                lFreqRangesRecon = await clientDB.Tables[NameTable.TableFreqRangesRecon].LoadAsync<TableFreqRanges>();
                ucFreqRangesRecon.UpdateFreqRanges(lFreqRangesRecon);
            }
            catch (ClientDataBase.ExceptionClient exeptClient)
            {
                MessageBox.Show(exeptClient.Message);
            }
            catch (ClientDataBase.ExceptionDatabase excpetService)
            {
                MessageBox.Show(excpetService.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
