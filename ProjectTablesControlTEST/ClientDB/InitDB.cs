﻿using System;
using ClientDataBase;
using InheritorsEventArgs;
using KirasaModelsDBLib;

namespace ProjectTablesControlTEST
{
    public partial class MainWindow
    {
        ClientDB clientDB;
        string endPoint = "127.0.0.1:8302";

        void InitClientDB()
        {
            clientDB.OnConnect += HandlerConnect;
            clientDB.OnDisconnect += HandlerDisconnect;
            clientDB.OnUpData += HandlerUpData;
            clientDB.OnErrorDataBase += HandlerErrorDataBase;

            (clientDB.Tables[NameTable.TableUAVRes] as ITableUpdate<TableUAVRes>).OnUpTable += OnUpTable_TableUAVRes;
            (clientDB.Tables[NameTable.TableUAVTrajectory] as ITableUpdate<TableUAVTrajectory>).OnUpTable += OnUpTable_TableUAVTrajectory;
            (clientDB.Tables[NameTable.TableLocalPoints] as ITableUpdate<TableLocalPoints>).OnUpTable += OnUpTable_TableLocalPoints;
            (clientDB.Tables[NameTable.TableRemotePoints] as ITableUpdate<TableRemotePoints>).OnUpTable += OnUpTable_TableRemotePoints;
            (clientDB.Tables[NameTable.TableFreqKnown] as ITableUpdate<TableFreqKnown>).OnUpTable += OnUpTable_TableFreqKnown;
            (clientDB.Tables[NameTable.TableFreqRangesRecon] as ITableUpdate<TableFreqRangesRecon>).OnUpTable += OnUpTable_TableFreqRangesRecon;
            (clientDB.Tables[NameTable.TableАeroscope] as ITableUpdate<TableAeroscope>).OnUpTable += OnUpTable_TableAeroscope;
            (clientDB.Tables[NameTable.TableАeroscopeTrajectory] as ITableUpdate<TableAeroscopeTrajectory>).OnUpTable += OnUpTable_TableAeroscopeTrajectory;
        }

       
    }
}
